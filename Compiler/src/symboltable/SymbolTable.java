
package symboltable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import syntaxtree.DataType;

/**
 * The SymbolTable stores informations about identifiers.
 * 
 * @author Troy Alcaraz
 */
public class SymbolTable
{
    //Single SymbolTable working as a Global SymbolTable
    HashMap<String,Symbol> symbolTable = new HashMap();
    
    /**
     * Internal Class Symbol is the object that
     * will be stored in the SymbolTable
     */
    public class Symbol
    {
        private String   lexeme;
        private Kind     kind;
        private DataType type;
        
        //Constructor
        public Symbol(String s, Kind k, DataType t)
        {
            lexeme = s;
            kind = k;
            type = t;
        }

        public String getLexeme() {
            return lexeme;
        }

        public Kind getKind() {
            return kind;
        }
        
        public DataType getDataType() {
            return type;
        }
        
        public Symbol() {}

        public void setLexeme(String lexeme) {
            this.lexeme = lexeme;
        }

        public void setKind(Kind kind) {
            this.kind = kind;
        }
        
        public void setDataType(DataType type) {
            this.type = type;
        }
        
        @Override
        public String toString()
        {
            return lexeme + " : " + kind + " : " + type;
        }
    }
    
    /**
     * Adds a Symbol to the Global SymbolTable and returns the lexeme
     * if the add is successful
     * @param s a Symbol
     */
    public void add(Symbol s)
    {
        if(!symbolTable.containsKey(s.lexeme))
        {
            symbolTable.put(s.lexeme, s);
        }
    }
    
    /**
     * Returns the Kind of a Symbol if it is in the SymbolTable
     * @param s a Symbol
     * @return the the Kind of a Symbol if it is in the
     * SymbolTable, else returns a message.
     */
    public String kind(String s)
    {
        if(symbolTable.containsKey(s))
        {
            return "" + symbolTable.get(s).kind;
        }
        else
        {
            return "The Symbol is not in the SymbolTable";
        }
    }
    
    public DataType dataType(String s) {
        return symbolTable.get(s).type;
    }
    
    public Symbol get(String s) {
        return symbolTable.get(s);
    }
    
    public boolean contains(String symbolName) {
        return symbolTable.containsKey(symbolName);
    }
    
    public ArrayList<Symbol> getSymbols() {
        ArrayList<Symbol> list = new ArrayList<>();
        
        for(Symbol symbol : symbolTable.values()) {
            list.add(symbol);
        }
        
        return list;
    }
    
    @Override
    public String toString()
    {
        String table = "";
        for(Symbol symbol : this.getSymbols()) {
            String s = String.format("%-15s %-15s %-15s",
                    symbol.getLexeme(),
                    symbol.getKind(),
                    symbol.getDataType());
            table += s + "\n";
        }
        
        return table;
    }
}
