
package symboltable;

/**
 *
 * @author Troy Alcaraz
 */
public enum Kind {
    PROGRAM,
    FUNCTION,
    ARRAY,
    VARIABLE,
}
