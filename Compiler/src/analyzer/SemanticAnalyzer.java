
package analyzer;

import symboltable.SymbolTable;
import syntaxtree.*;

/**
 *
 * @author Troy Alcaraz
 */
public class SemanticAnalyzer {
    
    private boolean canWriteAssembly = true;
    private ProgramNode program; 
    private SymbolTable table;

    public SemanticAnalyzer(ProgramNode pn, SymbolTable st) {
        program = pn; 
        table = st;
    }
    
    /**
     * Holds a Boolean value that represents if a program can write assembly code
     * based off of the ProgramNode and symbol table it is given
     * @return 
     */
    public boolean getCanWriteAssembly() {
        return canWriteAssembly;
    }
    
    /**
     * Checks that all ExpressionNodes have been delcared and are in the symbol
     * table
     */
    public void checkDeclarations() {
        for(VariableNode variable : program.getMain().getDeclarations().getVariables()) {
            checkDeclarations(variable);
        }
        
        for(StatementNode statement : program.getMain().getStatements().getStatements()) {
            checkDeclarations(statement);
        }
    }
    
    /**
     * Assigns data types to all ExpressionNodes
     */
    public void assignDataType() {
        
        for (VariableNode variable : program.getMain().getDeclarations().getVariables()) {
            assignDataType(variable);
        }
        
        for (StatementNode statement : program.getMain().getStatements().getStatements()) {
            assignDataType(statement);
        }
    }

    /**
     * Check that all the assigned data types match or are compatible
     */
    public void checkAssignmentTypes() {
        
        for(StatementNode statement : program.getMain().getStatements().getStatements()) {
            checkAssignmentTypes(statement);
        }
    }
    ///////////////////////////////
    //     Helper Functions      //
    ///////////////////////////////

    /**
     * Determines what kind of ExpressionNode the given expression is and checks if
     * its ExpressionNodes and VariableNodes have been declared
     * @param expNode 
     */
    private void checkDeclarations(ExpressionNode expNode) {
        if (expNode instanceof VariableNode) {
            
            VariableNode var = (VariableNode) expNode;
            checkDeclarations(var);
        }
        else if (expNode instanceof ValueNode) {
            // Do nothing
        }
        else if (expNode instanceof OperationNode) {
            
            OperationNode op = (OperationNode) expNode;
            checkDeclarations(op.getLeft());
            checkDeclarations(op.getRight());
        }
    }
    
    /**
     * Determines what kind of StatementNode the given statement is and checks if
     * its ExpressionNodes and VariableNodes have been declared
     * @param s 
     */
    private void checkDeclarations(StatementNode s) {
        if (s instanceof AssignmentStatementNode) {
            AssignmentStatementNode asn = (AssignmentStatementNode) s;
            checkDeclarations(asn.getExpression());
            checkDeclarations(asn.getLvalue());
        } 
        else if (s instanceof CompoundStatementNode) {
            CompoundStatementNode csn = (CompoundStatementNode) s;
            DeclarationsNode dn = csn.getDeclarations();
            for (VariableNode variable : dn.getVariables()) {
                checkDeclarations(variable);
            }
        } 
        else if (s instanceof IfStatementNode) {
            IfStatementNode isn = (IfStatementNode) s;
            checkDeclarations(isn.getTest());
            checkDeclarations(isn.getThenStatement());
            checkDeclarations(isn.getElseStatement());
        } 
        else if (s instanceof ProcedureStatementNode) {
            ProcedureStatementNode psn = (ProcedureStatementNode) s;
            for(ExpressionNode en : psn.getParameters().getExpressions()) {
                checkDeclarations(en);
            }
            
        } 
        else if (s instanceof ReturnStatementNode) {
            ReturnStatementNode rsn = (ReturnStatementNode) s;
            checkDeclarations(rsn.getReturnExp());
        } 
        else if (s instanceof WhileStatementNode) {
            WhileStatementNode whsn = (WhileStatementNode) s;
            checkDeclarations(whsn.getTest());
            checkDeclarations(whsn.getDoStatement());
        } 
        else if (s instanceof WriteStatementNode) {
            WriteStatementNode wrsn = (WriteStatementNode) s;
            checkDeclarations(wrsn.getWriteExp());
        }
    }
    
    /**
     * Checks if the given VariableNode has been declared by checking if it is in
     * the symbol table and sets the canWriteAssembly value based  off of what it
     * finds
     * @param vn The VariableNode to check in the symbol table for
     */
    private void checkDeclarations(VariableNode vn) {
        if(table.contains(vn.getName())) {
            // Do nothing
        }
        else {
            System.out.println("Unrecognized identifier: " + vn.getName());
            canWriteAssembly = false && canWriteAssembly;
        }
    }
    
    /**
     * Recursively move through statements and assign data types to their
     * ExpressionNodes and VariableNodes
     * @param s the statement to move through
     */
    private void assignDataType(StatementNode s) {

        if (s instanceof AssignmentStatementNode) {
            AssignmentStatementNode asn = (AssignmentStatementNode) s;
            assignDataType(asn.getExpression());
            assignDataType(asn.getLvalue());
        }  
        else if (s instanceof IfStatementNode) {
            IfStatementNode isn = (IfStatementNode) s;
            assignDataType(isn.getTest());
            assignDataType(isn.getThenStatement());
            assignDataType(isn.getElseStatement());
        } 
        else if (s instanceof ProcedureStatementNode) {
            ProcedureStatementNode psn = (ProcedureStatementNode) s;
            for(ExpressionNode en : psn.getParameters().getExpressions()) {
                assignDataType(en);
            }
            
        } 
        else if (s instanceof ReturnStatementNode) {
            ReturnStatementNode rsn = (ReturnStatementNode) s;
            assignDataType(rsn.getReturnExp());
        } 
        else if (s instanceof WhileStatementNode) {
            WhileStatementNode whsn = (WhileStatementNode) s;
            assignDataType(whsn.getTest());
            assignDataType(whsn.getDoStatement());
        } 
        else if (s instanceof WriteStatementNode) {
            WriteStatementNode wrsn = (WriteStatementNode) s;
            assignDataType(wrsn.getWriteExp());
        }
        else if (s instanceof CompoundStatementNode) {
            CompoundStatementNode csn = (CompoundStatementNode) s;
            for(VariableNode variable : csn.getDeclarations().getVariables()) {
                assignDataType(variable);
            }
            for(StatementNode statement : csn.getStatements().getStatements()) {
                assignDataType(statement);
            }
        }
    }

    /**
     * Assign data types to given ExpressionNode(s)
     * @param expNode the expression to assign a data type
     */
    private void assignDataType(ExpressionNode expNode) {
        
        if (expNode instanceof VariableNode) {
            
            VariableNode var = (VariableNode) expNode;
            assignDataType(var);
        } 
        else if (expNode instanceof OperationNode) {
            
            OperationNode op = (OperationNode) expNode;
            assignDataType(op.getLeft());
            assignDataType(op.getRight());
            if(op.getLeft().getType() == DataType.INT || op.getRight().getType() == DataType.INT) {
                op.setType(DataType.INT);
            }
            else {op.setType(DataType.FLOAT); }
        }
        else if (expNode instanceof ValueNode) {
            
            ValueNode val = (ValueNode) expNode;
            
            if(val.getAttribute().contains(".")) {
                val.setType(DataType.FLOAT);
            }
            else {
                val.setType(DataType.INT);
            }            
        }
    }
    
    /**
     * Assign data type for the given VariableNode if it has already been declared
     * in the symbol table
     * @param vn the VariableNode to assign a data type
     */
    private void assignDataType(VariableNode vn) {
        if(table.contains(vn.getName())) {
            vn.setType(table.dataType(vn.getName()));
        }
        else {
            System.out.println("Can't assign a data type: " + vn.getName());
            canWriteAssembly = false && canWriteAssembly;
        }
    }
    
    /**
     * Check if the given statement is an AssignmentStatementNode and if so
     * check that it's lValue and expression are compatible
     * @param statement 
     */
    private void checkAssignmentTypes(StatementNode statement) {
        
        if (statement instanceof AssignmentStatementNode) {
            AssignmentStatementNode asn = (AssignmentStatementNode) statement;
            checkAssignmentTypes(asn.getLvalue(), asn.getExpression());
        }
    }

    /**
     * Checks that the given VariableNode's data type matches or is compatible
     * with the data type of the given ExpressionNode
     * @param varNode an AssignmentStatementNode's lValue
     * @param expNode an AssignmentStatementNode's expression
     */
    private void checkAssignmentTypes(VariableNode varNode, ExpressionNode expNode) {
        
        if ((varNode.getType() == DataType.INT) && (expNode.getType() == DataType.FLOAT)) {
            System.out.println("Mismatched type: " + varNode.getName());
            canWriteAssembly = false && canWriteAssembly;
        }
    }
}
