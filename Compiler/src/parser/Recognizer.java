package parser;

import symboltable.Kind;
import symboltable.SymbolTable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import scanner.Scanner;
import scanner.Token;
import syntaxtree.TokenType;
import symboltable.SymbolTable.Symbol;
import syntaxtree.DataType;

/**
 * The recognizer determines whether an input of string tokens
 * is an expression.
 * To use a recognizer, create an instance pointing at a file,
 * and then call the top-level function, <code>program()</code>.
 * If the function returns without an error, the file
 * contains an acceptable expression
 *
 * @author Troy Alcaraz
 */
public class Recognizer {
    
    //Instance Variables

    /**
     * The Lookahead.
     */
    public Token lookahead;
    
    private Scanner scanner;

    /**
     * The Sym table.
     */
    public SymbolTable symTable;

    /**
     * The Sym.
     */
    public SymbolTable.Symbol sym;
    
    //Constructor

    /**
     * Instantiates a new Recognizer.
     *
     * @param text       the text
     * @param isFilename the is filename
     */
    public Recognizer( String text, boolean isFilename)
    {
        symTable = new SymbolTable();
        sym = symTable.new Symbol();
        
        if(isFilename)
        {
            FileInputStream fis = null;
            try
            {
                fis = new FileInputStream(text);
            }
            catch (FileNotFoundException ex)
            {
                error("No File");
            }
            
            InputStreamReader isr = new InputStreamReader(fis);
            scanner = new Scanner(isr);
        }
        else
        {
            scanner = new Scanner( new StringReader(text));
        }
        try {
            lookahead = scanner.next();
        }
        catch (IOException ex)
        {
            error("Scan error");
        }
    }
    
    //Methods

    /**
     * Executes the rule for the program non-terminal symbol in
     * the expression grammar.
     */
    public void program()
    {
        functionDeclarations();
        match(TokenType.MAIN);
        match(TokenType.LPER);
        match(TokenType.RPER);
        compoundStatement();
        functionDefinitions();
    }

    /**
     * Executes the rule for the functionDeclarations non-terminal symbol in
     * the expression grammar.
     */
    public void functionDeclarations() {
        if(isType(lookahead))
        {
            functionDeclaration();
            match(TokenType.SEMICOLEN);
            functionDeclarations();
        }
        else
        {
            //Lambda Option
        }
    }

    /**
     * Executes the rule for the compoundStatement non-terminal symbol in
     * the expression grammar.
     */
    public void compoundStatement() {
        if(lookahead.getType() == TokenType.LCURLY)
        {
            match(TokenType.LCURLY);
            declarations();
            optionalStatements();
            match(TokenType.RCURLY);
        }
        else
        {
            error("Compound Statement");
        }
    }

    /**
     * Executes the rule for the functionDefinitions non-terminal symbol in
     * the expression grammar.
     */
    public void functionDefinitions() {
        if(isType(lookahead))
        {
            functionDefinition();
            functionDefinitions();
        }
        else
        {
            //Lambda Option
        }
    }

    /**
     * Executes the rule for the identifierList non-terminal symbol in
     * the expression grammar.
     */
    public void identifierList(DataType type) {
        if(isID(lookahead))
        {
            sym = symTable.new Symbol(lookahead.getLexeme(), Kind.VARIABLE, type);
            match(TokenType.IDENTIFIER);
            symTable.add(sym);
            if(lookahead.getType() == TokenType.COMMA)
            {
                match(TokenType.COMMA);
                identifierList(type);
            }
        }
        else
        {
            error("Identifier List");
        }
    }

    /**
     * Executes the rule for the declarations non-terminal symbol in
     * the expression grammar.
     */
    public void declarations() {
        if(isType(lookahead))
        {
            DataType type = type();
            identifierList(type);
            match(TokenType.SEMICOLEN);
            declarations();
        }
        else
        {
            //Lambda Option
        }
    }

    /**
     * Executes the rule for the type non-terminal symbol in
     * the expression grammar.
     */
    public DataType type() {
        DataType answer = null;
        
        switch(lookahead.getType())
        {
            case VOID:
                match(TokenType.VOID);
                answer = DataType.VOID;
                break;
            case INT:
                match(TokenType.INT);
                answer = DataType.VOID;
                break;
            case FLOAT:
                match(TokenType.FLOAT);
                answer = DataType.FLOAT;
                break;
            default:
                error("Type");
                break;
        }
        return answer;
    }

    /**
     * Executes the rule for the functionDeclaration non-terminal symbol in
     * the expression grammar.
     */
    public void functionDeclaration() {
        if(isType(lookahead))
        {
            DataType type = type();
            sym = symTable.new Symbol(lookahead.getLexeme(), Kind.FUNCTION, type);
            match(TokenType.IDENTIFIER);
            symTable.add(sym);
            parameters();
        }
        else
        {
            error("Function Declaration");
        }
    }

    /**
     * Executes the rule for the functionDefinition non-terminal symbol in
     * the expression grammar.
     */
    public void functionDefinition() {
        if(isType(lookahead))
        {
            type();
            match(TokenType.IDENTIFIER);
            parameters();
            compoundStatement();
        }
        else
        {
            error("Function Definition");
        }
    }

    /**
     * Executes the rule for the parameters non-terminal symbol in
     * the expression grammar.
     */
    public void parameters() {
        if(lookahead.getType() == TokenType.LPER)
        {
            match(TokenType.LPER);
            parameterList();
            match(TokenType.RPER);
        }
        else
        {
            error("Parameters");
        }
    }

    /**
     * Executes the rule for the parameterList non-terminal symbol in
     * the expression grammar.
     */
    public void parameterList() {
        if(isType(lookahead))
        {
            DataType type = type();
            sym = symTable.new Symbol(lookahead.getLexeme(), Kind.VARIABLE, type);
            match(TokenType.IDENTIFIER);
            symTable.add(sym);
            if(lookahead.getType() == TokenType.COMMA)
            {
                match(TokenType.COMMA);
                parameterList();
            }
        }
        else
        {
            //Lambda Option
        }
    }

    /**
     * Executes the rule for the optionalStatements non-terminal symbol in
     * the expression grammar.
     */
    public void optionalStatements() {
        if(isStatement(lookahead))
        {
            statementList();
        }
        else
        {
            //Lambda Option
        }
    }

    /**
     * Executes the rule for the statementList non-terminal symbol in
     * the expression grammar.
     */
    public void statementList() {
        if(isStatement(lookahead))
        {
            statement();
            match(TokenType.SEMICOLEN);
            if(isStatement(lookahead))
            {
                statementList();
            }
        }
    }

    /**
     * Executes the rule for the statement non-terminal symbol in
     * the expression grammar.
     */
    public void statement() {
        switch(lookahead.getType())
        {
            case IDENTIFIER:
                if(symTable.kind(sym.getLexeme()).equals("VARIABLE"))
                {
                    variable();
                    match(TokenType.EQUAL);
                    expression();
                }
                else
                {
                    procedureStatement();
                }
                break;
            case LCURLY:
                compoundStatement();
                break;
            case IF:
                match(TokenType.IF);
                expression();
                statement();
                match(TokenType.ELSE);
                statement();
                break;
            case WHILE:
                match(TokenType.WHILE);
                expression();
                statement();
                break;
            case READ:
                match(TokenType.READ);
                match(TokenType.LPER);
                match(TokenType.IDENTIFIER);
                match(TokenType.RPER);
                break;
            case WRITE:
                match(TokenType.WRITE);
                match(TokenType.LPER);
                expression();
                match(TokenType.RPER);
                break;
            case RETURN:
                match(TokenType.RETURN);
                expression();
                break;
            default:
                error("Statement");
                break;
        }
    }

    /**
     * Procedure statement.
     */
    public void procedureStatement() {
        if(isID(lookahead)) {
            match(TokenType.IDENTIFIER);
            if(lookahead.getType() == TokenType.LPER)
            {
                match(TokenType.LPER);
                expressionList();
                match(TokenType.RPER);
            }
        }
        else
        {
            error("ProcedureStatement");
        }
    }

    /**
     * Executes the rule for the variable non-terminal symbol in
     * the expression grammar.
     */
    public void variable() {
        if(isID(lookahead))
        {
            match(TokenType.IDENTIFIER);
            if(lookahead.getType() == TokenType.LBRAKET)
            {
                match(TokenType.LBRAKET);
                expression();
                match(TokenType.RBRAKET);
            }
        }
        else
        {
            error("Variable");
        }
    }

    /**
     * Executes the rule for the term non-terminal symbol in
     * the expression grammar.
     */
    public void term()
    {
        if(isFactor(lookahead))
        {
            factor();
            termPart();
        }
        else
        {
            error("Term");
        }
    }

    /**
     * Executes the rule for the factor non-terminal symbol in
     * the expression grammar.
     */
    public void factor()
    {
        switch(lookahead.getType())
        {
            case IDENTIFIER:
                match(TokenType.IDENTIFIER);
                if(lookahead.getType() == TokenType.LBRAKET)
                {
                    match(TokenType.LBRAKET);
                    expression();
                    match(TokenType.RBRAKET);
                }
                else if(lookahead.getType() == TokenType.LPER)
                {
                    match(TokenType.LPER);
                    expressionList();
                    match(TokenType.RPER);
                }
                break;
            case NUMBER:
                match(TokenType.NUMBER);
                break;
            case LPER:
                match(TokenType.LPER);
                expression();
                match(TokenType.RPER);
                break;
            case NOT:
                match(TokenType.NOT);
                factor();
                break;
            default:
                error("Factor");
                break;
        }
    }

    /**
     * Executes the rule for the term part non-terminal symbol in
     * the expression grammar.
     */
    public void termPart()
    {
        if (isMulop(lookahead))
        {
            mulop();
            factor();
            termPart();
        }
        else
        {
            // Lambda option
        }
    }

    /**
     * Executes the rule for the expression non-terminal symbol in
     * the expression grammar.
     */
    public void expression() {
        simpleExpression();
        if(isRelop(lookahead))
        {
            match(lookahead.getType());
            simpleExpression();
        }
    }

    /**
     * Executes the rule for the expression list non-terminal symbol in
     * the expression grammar.
     */
    public void expressionList() {
        simpleExpression();
        if(lookahead.getType() == TokenType.COMMA)
        {
            match(TokenType.COMMA);
            simpleExpression();
        }
        else
        {
            error("ExpressionList");
        }
    }

    /**
     * Executes the rule for the simple expression non-terminal symbol in
     * the expression grammar.
     */
    public void simpleExpression() {
        if(isSign(lookahead))
        {
            sign();
            term();
            simplePart();
        }
        else if(!isSign(lookahead))
        {
            term();
            simplePart();
        }
        else
        {
            error("SimpleExpression");
        }
    }

    /**
     * Executes the rule for the simple part non-terminal symbol in
     * the expression grammar.
     */
    public void simplePart() {
        if(isAddop(lookahead))
        {
            addop();
            term();
            simplePart();
        }
        else
        {
            // Lambda option
        }
    }
    
    /**
     * Determines whether or not the given token is
     * a id token.
     * @param token The token to check.
     * @return true if the token is a id, false otherwise
     */
    private boolean isID(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.IDENTIFIER)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a number token.
     * @param token The token to check.
     * @return true if the token is a number, false otherwise
     */
    private boolean isNum(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.NUMBER)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a factor.
     * @param token The token to check.
     * @return true if the token is a factor, false otherwise
     */
    private boolean isFactor(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.IDENTIFIER ||
                token.getType() == TokenType.NUMBER ||
                token.getType() == TokenType.LPER ||
                token.getType() == TokenType.NOT)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a statement.
     * @param token The token to check.
     * @return true if the token is a statement, false otherwise
     */
    private boolean isStatement(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.IDENTIFIER ||
                token.getType() == TokenType.LCURLY ||
                token.getType() == TokenType.IF ||
                token.getType() == TokenType.WHILE ||
                token.getType() == TokenType.READ ||
                token.getType() == TokenType.WRITE ||
                token.getType() == TokenType.RETURN)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a mulop token.
     * @param token The token to check.
     * @return true if the token is a mulop, false otherwise
     */
    private boolean isMulop(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.MULTIPLY ||
                token.getType() == TokenType.DIVIDE ||
                token.getType() == TokenType.MOD ||
                token.getType() == TokenType.AND)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a addop token.
     * @param token The token to check.
     * @return true if the token is a addop, false otherwise
     */
    private boolean isAddop(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.PLUS ||
                token.getType() == TokenType.MINUS ||
                token.getType() == TokenType.OR)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a relop token.
     * @param token The token to check.
     * @return true if the token is a relop, false otherwise
     */
    private boolean isRelop(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.EQUAL2 ||
                token.getType() == TokenType.NOTEQUAL ||
                token.getType() == TokenType.LESSTHAN ||
                token.getType() == TokenType.LESSEQUAL ||
                token.getType() == TokenType.GREATEQUAL ||
                token.getType() == TokenType.GREATTHAN)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a sign token.
     * @param token The token to check.
     * @return true if the token is a sign, false otherwise
     */
    private boolean isSign(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.PLUS ||
                token.getType() == TokenType.MINUS)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a type.
     * @param token The token to check
     * @return true if the token is a type, false otherwise
     */
    private boolean isType(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.VOID ||
                token.getType() == TokenType.INT ||
                token.getType() == TokenType.FLOAT)
        {
            answer = true;
        }
        return answer;
    }

    /**
     * Executes the rule for the sign non-terminal symbol in
     * the expression grammar.
     */
    public void sign()
    {
        if(lookahead.getType() == TokenType.PLUS)
        {
            match( TokenType.PLUS);
        }
        else if(lookahead.getType() == TokenType.MINUS)
        {
            match( TokenType.MINUS);
        }
        else
        {
            error("Sign");
        }
    }

    /**
     * Addop.
     */
    public void addop()
    {
        switch(lookahead.getType())
        {
            case PLUS:
                match(TokenType.PLUS);
                break;
            case MINUS:
                match(TokenType.MINUS);
                break;
            case OR:
                match(TokenType.OR);
                break;
            default:
                error("Addop");
                break;
        }
    }

    /**
     * Executes the rule for the mulop non-terminal symbol in
     * the expression grammar.
     */
    public void mulop()
    {        
        switch(lookahead.getType())
        {
            case MULTIPLY:
                match(TokenType.MULTIPLY);
                break;
            case DIVIDE:
                match(TokenType.DIVIDE);
                break;
            case MOD:
                match(TokenType.MOD);
                break;
            case AND:
                match(TokenType.AND);
                break;
            default:
                error("Mulop");
                break;
        }
    }

    /**
     * Is at eo f boolean.
     *
     * @param token the token
     * @return the boolean
     */
    public boolean isAtEoF(Token token)
    {
        boolean answer = false;
        
        if(token.getLexeme().equals("End of File") && token.getType() == null)
        {
            answer = true;
        }
        return answer;
    }

    /**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     *
     * @param expected the expected token type.
     */
    public void match( TokenType expected)
    {
        //System.out.println("match( " + expected + " )");
        if(this.lookahead.getType() == expected)
        {
            try
            {
                this.lookahead = scanner.next();
                if (this.lookahead == null)
                {
                    this.lookahead = new Token( "End of File", null);
                }
            }
            catch (IOException ex)
            {
                error( "Scanner exception");
            }
        }
        else
        {
            error("trying to Match " + expected + ", but found " + 
                    this.lookahead.getType() + " instead.");
        }
    }

    /**
     * Errors out of the Recognizer
     * Prints an error message and then exits the program
     *
     * @param message The error message to print
     */
    public void error( String message)
    {
        throw new RuntimeException( "Error " + message + " at line " + this.scanner.getLine() + " column " + this.scanner.getColumn());
    }
}
