package parser;

import symboltable.Kind;
import symboltable.SymbolTable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import scanner.Scanner;
import scanner.Token;
import syntaxtree.TokenType;
import symboltable.SymbolTable.Symbol;
import syntaxtree.*;

/**
 *The recognizer determines whether an input of string tokens
 * is an expression.
 * To use a recognizer, create an instance pointing at a file,
 * and then call the top-level function, <code>program()</code>.
 * If the function returns without an error, the file
 * contains an acceptable expression
 * 
 * @author Troy Alcaraz
 */
public class Parser {
    
    //Instance Variables
    
    public Token lookahead;
    
    private Scanner scanner;
    
    public SymbolTable symTable;
    
    public SymbolTable.Symbol sym;
    
    //Constructor
    
    public Parser( String text, boolean isFilename)
    {
        symTable = new SymbolTable();
        sym = symTable.new Symbol();
        
        if(isFilename)
        {
            FileInputStream fis = null;
            try
            {
                fis = new FileInputStream(text);
            }
            catch (FileNotFoundException ex)
            {
                error("No File");
            }
            
            InputStreamReader isr = new InputStreamReader(fis);
            scanner = new Scanner(isr);
        }
        else
        {
            scanner = new Scanner( new StringReader(text));
        }
        try {
            lookahead = scanner.next();
        }
        catch (IOException ex)
        {
            error("Scan error");
        }
    }
    
    //Methods
    
    /**
     * Executes the rule for the program non-terminal symbol in
     * the expression grammar.
     * @return a ProgramNode
     */
    public ProgramNode program()
    {
        ProgramNode answer = new ProgramNode();
        
        FunctionsDecNode functionDeclarations = functionDeclarations();
        match(TokenType.MAIN);
        match(TokenType.LPER);
        match(TokenType.RPER);
        CompoundStatementNode statement = compoundStatement();
        FunctionsDefNode functionDefinitions = functionDefinitions();
        
        answer.setMain(statement);
        answer.setFunctions(functionDefinitions);
        
        return answer;
    }
    
    /**
     * Executes the rule for the functionDeclarations non-terminal symbol in
     * the expression grammar.
     * @return a FunctionsDecNode
     */
    public FunctionsDecNode functionDeclarations() {
        FunctionsDecNode answer = new FunctionsDecNode();
                
        DataType type = type();
        
        if(isType(type))
        {
            FunctionDecNode funcDec = functionDeclaration(type);
            match(TokenType.SEMICOLEN);
            FunctionsDecNode funcsDec = functionDeclarations();
            
            answer.addFunctionDeclaration(funcDec);
            for( FunctionDecNode aFuncDec : funcsDec.getFunctions()) {
                answer.addFunctionDeclaration(aFuncDec);
            }
        }
        else
        {
            //Lambda Option
        }
        return answer;
    }
    
    /**
     * Executes the rule for the functionDeclaration non-terminal symbol in
     * the expression grammar.
     * @param type
     * @return a FunctionDecNode
     */
    public FunctionDecNode functionDeclaration(DataType type) {
        FunctionDecNode answer = null;
                        
        if(isID(lookahead))
        { 
            sym = symTable.new Symbol(lookahead.getLexeme(), Kind.FUNCTION, type);
            answer = new FunctionDecNode(lookahead.getLexeme());
            match(TokenType.IDENTIFIER);            
            symTable.add(sym);
            ParametersNode someParameters = parameters();
            
            answer.setReturnType(type);
            for( VariableNode aParameter : someParameters.getParameters()) {
                answer.addParameter(aParameter);
            }
        }
        else
        {
            error("Function Declaration");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the functionDefinitions non-terminal symbol in
     * the expression grammar.
     * @return a FunctionsNode
     */
    public FunctionsDefNode functionDefinitions() {
        FunctionsDefNode answer = new FunctionsDefNode();
        
        if(isType(lookahead.getType()))
        {
            FunctionDefNode aFunc = functionDefinition();
            answer.addFunctionDefinition(aFunc);
            
            FunctionsDefNode someFuncs = functionDefinitions();
            for( FunctionDefNode someFunc : someFuncs.getFunctions()) {
                answer.addFunctionDefinition(someFunc);
            }
        }
        else
        {
            //Lambda Option
        }
        return answer;
    }
    
    /**
     * Executes the rule for the functionDefinition non-terminal symbol in
     * the expression grammar.
     * @return a FunctionNode
     */
    public FunctionDefNode functionDefinition() {
        FunctionDefNode answer = null;
        DataType type = type();
        
        if(isType(type))
        {   
            answer = new FunctionDefNode(lookahead.getLexeme());
            
            match(TokenType.IDENTIFIER);
            ParametersNode someParameters = parameters();
            CompoundStatementNode someStatement = compoundStatement();
            
            answer.setReturnType(type);
            answer.setBody(someStatement);
            for (VariableNode parameter : someParameters.getParameters()) {
                answer.addParameter(parameter);
            }
            
        }
        else
        {
            error("Function Definition");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the compoundStatement non-terminal symbol in
     * the expression grammar.
     * @return a CompoundStatementNode
     */
    public CompoundStatementNode compoundStatement() {
        CompoundStatementNode answer = new CompoundStatementNode();
        if(lookahead.getType() == TokenType.LCURLY)
        {
            match(TokenType.LCURLY);
            DeclarationsNode aDeclaration = declarations();
            StatementListNode aStatementList = optionalStatements();
            
            answer.setVariables(aDeclaration);
            
            for (StatementNode possibleStatement : aStatementList.getStatements()) {
                answer.addStatement(possibleStatement);
            }
            
            match(TokenType.RCURLY);
        }
        else
        {
            error("Compound Statement");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the declarations non-terminal symbol in
     * the expression grammar.
     * @return a DeclarationNode
     */
    public DeclarationsNode declarations() {
        DeclarationsNode answer = new DeclarationsNode();
        
        DataType type = type();
                
        if(isType(type))
        {
            IdentifierListNode idList = identifierList(type);
            
            for(VariableNode possibleVar : idList.getIDs()) {
                answer.addVariable(possibleVar);
            }
            match(TokenType.SEMICOLEN);
            DeclarationsNode d = declarations();
            for(VariableNode possibleVar : d.getVariables()) {
                answer.addVariable(possibleVar);
            }
        }
        else
        {
            //Lambda Option
        }
        return answer;
    }
    
    /**
     * Executes the rule for the type non-terminal symbol in
     * the expression grammar.
     * @return the TokenType
     */
    public DataType type() {
        DataType answer = null;
        
        if(!isAtEoF(lookahead)) {
            switch(lookahead.getType())
            {
                case VOID:
                    match(TokenType.VOID);
                    answer = DataType.VOID;
                    break;
                case INT:
                    match(TokenType.INT);
                    answer = DataType.INT;
                    break;
                case FLOAT:
                    match(TokenType.FLOAT);
                    answer = DataType.FLOAT;
                    break;
                default:
                    return answer;

            }
            return answer;
        }
        return answer;
    }
    
    /**
     * Executes the rule for the parameters non-terminal symbol in
     * the expression grammar.
     * @return 
     */
    public ParametersNode parameters() {
        ParametersNode answer = new ParametersNode();
        
        if(lookahead.getType() == TokenType.LPER)
        {
            match(TokenType.LPER);
            ArrayList<VariableNode> parameters = parameterList();
            
            for ( VariableNode aParameter : parameters) {
                answer.addParameter(aParameter);
            }
            
            match(TokenType.RPER);
        }
        else
        {
            error("Parameters");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the parameterList non-terminal symbol in
     * the expression grammar.
     * @return an ArrayList of VariableNodes
     */
    public ArrayList<VariableNode> parameterList() {
        ArrayList<VariableNode> vars = new ArrayList<>();
        
        DataType type = type();
        
        if(isType(type))
        {
            sym = symTable.new Symbol(lookahead.getLexeme(), Kind.VARIABLE, type);
            VariableNode aVar = new VariableNode(lookahead.getLexeme());
            match(TokenType.IDENTIFIER);
            symTable.add(sym);
            vars.add(aVar);
            if(lookahead.getType() == TokenType.COMMA)
            {
                match(TokenType.COMMA);
                ArrayList<VariableNode> moreParameters = parameterList();
                for (VariableNode possibleVar : moreParameters) {
                    vars.add(possibleVar);
                }
            }
        }
        else
        {
            //Lambda Option
        }
        return vars;
    }
    
    /**
     * Executes the rule for the optionalStatements non-terminal symbol in
     * the expression grammar.
     * @return a StatementList, containing an ArrayList of possible StatementNodes
     */
    public StatementListNode optionalStatements() {
        StatementListNode answer = new StatementListNode();
        
        if(isStatement(lookahead))
        {
            answer = statementList();
        }
        else
        {
            //Lambda Option
        }
        return answer;
    }
    
    /**
     * Executes the rule for the statementList non-terminal symbol in
     * the expression grammar.
     * @return a StatementListNode, containing an ArrayList of StatementNodes
     */
    public StatementListNode statementList() {
        StatementListNode answer = new StatementListNode();
        if(isStatement(lookahead))
        {
            StatementNode aStatement = statement();
            answer.addStatement(aStatement);
            match(TokenType.SEMICOLEN);
            if(isStatement(lookahead))
            {
                StatementListNode possibleList = statementList();
                
                for( StatementNode possibleStatement : possibleList.getStatements()) {
                    answer.addStatement(possibleStatement);
                }
            }
        }
        else
        {
            error("StatementList");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the expression list non-terminal symbol in
     * the expression grammar.
     * @return An ExpressionListNode, containing an ArrayList of ExpressionNodes
     */
    public ExpressionListNode expressionList() {
        ExpressionListNode answer = new ExpressionListNode();
        
        ExpressionNode anExpression = simpleExpression();
        answer.addExpression(anExpression);
        
        if(lookahead.getType() == TokenType.COMMA)
        {
            match(TokenType.COMMA);
            ExpressionListNode possibleList = expressionList();
            
            for( ExpressionNode possibleExp : possibleList.getExpressions()) {
                answer.addExpression(possibleExp);
            }
        }
        else
        {
            error("ExpressionList");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the identifierList non-terminal symbol in
     * the expression grammar.
     * @return an IdentifierListNode, containing an ArrayList of VariableNodes
     */
    public IdentifierListNode identifierList(DataType type) {
        IdentifierListNode answer = new IdentifierListNode();
        if(isID(lookahead))
        {
            VariableNode aVar = new VariableNode(lookahead.getLexeme());
            sym = symTable.new Symbol(lookahead.getLexeme(), Kind.VARIABLE, type);
            
            match(TokenType.IDENTIFIER);
            symTable.add(sym);
            answer.addID(aVar);
            if(lookahead.getType() == TokenType.COMMA)
            {
                match(TokenType.COMMA);
                IdentifierListNode possibleList = identifierList(type);
                
                for (VariableNode possibleVar : possibleList.getIDs()) {
                    answer.addID(possibleVar);
                }
            }
        }
        else
        {
            error("Identifier List");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the statement non-terminal symbol in
     * the expression grammar.
     * @return a StatementNode
     */
    public StatementNode statement() {
        
        StatementNode answer = null;
        
        switch(lookahead.getType())
        {
            case IDENTIFIER:
                if(symTable.kind(sym.getLexeme()).equals("VARIABLE"))
                {
                    AssignmentStatementNode asn = new AssignmentStatementNode();
                    VariableNode lv = variable();
                    match(TokenType.EQUAL);
                    ExpressionNode exp = expression();
                    asn.setLvalue(lv);
                    asn.setExpression(exp);
                    answer = asn;
                }
                else {
                    answer = procedureStatement();
                }
                break;
            case LCURLY:
                //TODO
                answer = compoundStatement();
                break;
            case IF:
                IfStatementNode isn = new IfStatementNode();
                match(TokenType.IF);
                ExpressionNode t = expression();
                StatementNode thenState = statement();
                match(TokenType.ELSE);
                StatementNode elseState = statement();
                isn.setTest(t);
                isn.setThenStatement(thenState);
                isn.setElseStatement(elseState);
                answer = isn;
                break;
            case WHILE:
                WhileStatementNode wsn = new WhileStatementNode();
                match(TokenType.WHILE);
                ExpressionNode t1 = expression();
                StatementNode doState = statement();
                wsn.setTest(t1);
                wsn.setDoStatement(doState);
                answer = wsn;
                break;
            case READ:
                ReadStatementNode rsn = new ReadStatementNode();
                match(TokenType.READ);
                match(TokenType.LPER);
                VariableNode vn = new VariableNode(lookahead.getLexeme());
                match(TokenType.IDENTIFIER);
                match(TokenType.RPER);
                rsn.setID(vn.getName());
                answer = rsn;
                break;
            case WRITE:
                WriteStatementNode wrsn = new WriteStatementNode();
                match(TokenType.WRITE);
                match(TokenType.LPER);
                ExpressionNode exp = expression();
                match(TokenType.RPER);
                wrsn.setWriteExp(exp);
                answer = wrsn;
                break;
            case RETURN:
                ReturnStatementNode resn = new ReturnStatementNode();
                match(TokenType.RETURN);
                ExpressionNode returnExp = expression();
                resn.setReturnExp(returnExp);
                answer = resn;
                break;
            default:
                error("Statement");
                break;
        }
        return answer;
    }
    
    public ProcedureStatementNode procedureStatement() {
        ProcedureStatementNode psn = new ProcedureStatementNode();
        if(isID(lookahead)) {
            psn.setProcedureID(lookahead.getLexeme());
            match(TokenType.IDENTIFIER);
            if(lookahead.getType() == TokenType.LPER)
            {
                match(TokenType.LPER);
                ExpressionListNode eln = expressionList();
                match(TokenType.RPER);
                psn.setParamenter(eln);
            }
        }
        else
        {
            error("ProcedureStatement");
        }
        return psn;
    }
    
    /**
     * Executes the rule for the variable non-terminal symbol in
     * the expression grammar.
     * @return a VariableNode
     */
    public VariableNode variable() {
        VariableNode answer = null;
        
        if(isID(lookahead))
        {
            answer = new VariableNode(this.lookahead.getLexeme());
            match(TokenType.IDENTIFIER);
            
            ///////////////////////////////
            // Don't worry about arrays yet
            ///////////////////////////////
            /*
            if(lookahead.getType() == TokenType.LBRAKET)
            {
                match(TokenType.LBRAKET);
                expression();
                match(TokenType.RBRAKET);
            }
            */
        }
        else
        {
            error("Variable");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the factor non-terminal symbol in
     * the expression grammar.
     * @return an ExpressionNode
     */
    public ExpressionNode factor()
    {
        ExpressionNode answer = null;
        switch(lookahead.getType())
        {
            case IDENTIFIER:
                Token vToken = this.lookahead;
                match(TokenType.IDENTIFIER);
                
                ///////////////////////////////
                // Don't worry about arrays yet
                ///////////////////////////////
                /*
                if(lookahead.getType() == TokenType.LBRAKET)
                {
                    answer = new VariableNode(vToken.getLexeme());
                    match(TokenType.LBRAKET);
                    expression();
                    match(TokenType.RBRAKET);
                }
                */
                
                if(lookahead.getType() == TokenType.LPER)
                {
                    answer = new VariableNode(vToken.getLexeme());
                    match(TokenType.LPER);
                    expressionList();
                    match(TokenType.RPER);
                }
                else 
                {
                    answer = new VariableNode(vToken.getLexeme());
                }
                break;
            case NUMBER:
                Token valueToken = this.lookahead;
                match(TokenType.NUMBER);
                answer = new ValueNode(valueToken.getLexeme());
                break;
            case LPER:
                match(TokenType.LPER);
                answer = expression();
                match(TokenType.RPER);
                break;
            case NOT:
                match(TokenType.NOT);
                factor();
                break;
            default:
                error("Factor");
                break;
        }
        return answer;
    }
    
    /**
     * Executes the rule for the expression non-terminal symbol in
     * the expression grammar.
     * @return An ExpressionNode
     */
    public ExpressionNode expression() {

        ExpressionNode anExp = simpleExpression();
        if(isRelop(lookahead))
        {
            
            OperationNode on = relop();
            ExpressionNode otherExp = simpleExpression();
            on.setLeft(anExp);
            on.setRight(otherExp);
            return on;
        }
        else
        {
            return anExp;
        }
    }
    
    /**
     * Executes the rule for the simple expression non-terminal symbol in
     * the expression grammar.
     * @return an ExpressionNode
     */
    public ExpressionNode simpleExpression() {
        if(isSign(lookahead))
        {
            sign();
            ExpressionNode aTerm = term();
            return simplePart(aTerm);
        }
        else
        {            
            ExpressionNode aTerm = term();
            return simplePart(aTerm);
        }
    }
    
    /**
     * Executes the rule for the simple part non-terminal symbol in
     * the expression grammar.
     * @param possibleLeft possibly the left term of an OperationNode or
     * a VariableNode or ValueNode
     * @return an ExpressionNode
     */
    public ExpressionNode simplePart(ExpressionNode possibleLeft) {
        if(isAddop(lookahead))
        {
            OperationNode on = addop();
            ExpressionNode right = term();
            on.setLeft(possibleLeft);
            on.setRight(right);
            return simplePart(on);
        }
        else
        {
            return possibleLeft;
            // Lambda option
        }
    }
    
    /**
     * Executes the rule for the term non-terminal symbol in
     * the expression grammar.
     * @return an ExpressionNode
     */
    public ExpressionNode term()
    {
        ExpressionNode answer = null;
        
        if(isFactor(lookahead)) {
            ExpressionNode left = factor();
            answer = termPart(left);
        }
        else {
            error("Term");
        }
        return answer;
    }
    
    /**
     * Executes the rule for the term part non-terminal symbol in
     * the expression grammar.
     * @param possibleLeft possibly the left term of an OperationNode or
     * a ValueNode or VariableNode
     * @return an ExpressionNode
     */
    public ExpressionNode termPart(ExpressionNode possibleLeft)
    {
        if (isMulop(lookahead))
        {
            OperationNode on = mulop();
            ExpressionNode right = factor();
            
            on.setLeft(possibleLeft);
            on.setRight(right);
            return termPart(on);
        }
        else
        {
            return possibleLeft;
            // Lambda option
        }
    }
    
    /**
     * Determines whether or not the given token is
     * an id token.
     * @param token The token to check.
     * @return true if the token is a id, false otherwise
     */
    private boolean isID(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.IDENTIFIER)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a number token.
     * @param token The token to check.
     * @return true if the token is a number, false otherwise
     */
    private boolean isNum(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.NUMBER)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a factor.
     * @param token The token to check.
     * @return true if the token is a factor, false otherwise
     */
    private boolean isFactor(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.IDENTIFIER ||
                token.getType() == TokenType.NUMBER ||
                token.getType() == TokenType.LPER ||
                token.getType() == TokenType.NOT)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a statement.
     * @param token The token to check.
     * @return true if the token is a statement, false otherwise
     */
    private boolean isStatement(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.IDENTIFIER ||
                token.getType() == TokenType.LCURLY ||
                token.getType() == TokenType.IF ||
                token.getType() == TokenType.WHILE ||
                token.getType() == TokenType.READ ||
                token.getType() == TokenType.WRITE ||
                token.getType() == TokenType.RETURN)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a mulop token.
     * @param token The token to check.
     * @return true if the token is a mulop, false otherwise
     */
    private boolean isMulop(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.MULTIPLY ||
                token.getType() == TokenType.DIVIDE ||
                token.getType() == TokenType.MOD ||
                token.getType() == TokenType.AND)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a addop token.
     * @param token The token to check.
     * @return true if the token is a addop, false otherwise
     */
    private boolean isAddop(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.PLUS ||
                token.getType() == TokenType.MINUS ||
                token.getType() == TokenType.OR)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a relop token.
     * @param token The token to check.
     * @return true if the token is a relop, false otherwise
     */
    private boolean isRelop(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.EQUAL2 ||
                token.getType() == TokenType.NOTEQUAL ||
                token.getType() == TokenType.LESSTHAN ||
                token.getType() == TokenType.LESSEQUAL ||
                token.getType() == TokenType.GREATEQUAL ||
                token.getType() == TokenType.GREATTHAN)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a sign token.
     * @param token The token to check.
     * @return true if the token is a sign, false otherwise
     */
    private boolean isSign(Token token)
    {
        boolean answer = false;
        if(token.getType() == TokenType.PLUS ||
                token.getType() == TokenType.MINUS)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Determines whether or not the given token is
     * a type.
     * @param token The token to check
     * @return true if the token is a type, false otherwise
     */
    private boolean isType(DataType type)
    {
        boolean answer = false;
        if(type == DataType.VOID ||
                type == DataType.INT ||
                type == DataType.FLOAT)
        {
            answer = true;
        }
        return answer;
    }
    
    private boolean isType(TokenType type) {
        boolean answer = false;
        if(type == TokenType.VOID ||
                type == TokenType.INT ||
                type == TokenType.FLOAT)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Executes the rule for the sign non-terminal symbol in
     * the expression grammar.
     */
    public void sign()
    {
        if(lookahead.getType() == TokenType.PLUS)
        {
            match( TokenType.PLUS);
        }
        else if(lookahead.getType() == TokenType.MINUS)
        {
            match( TokenType.MINUS);
        }
        else
        {
            error("Sign");
        }
    }
    
    /**
     * Executes the rule for the addop non-terminal symbol in
     * the expression grammar.
     * @return an OperationNode with a addop operation type
     */
    public OperationNode addop()
    {
        OperationNode answer = null;
        switch(lookahead.getType())
        {
            case PLUS:
                match(TokenType.PLUS);
                answer = new OperationNode(TokenType.PLUS);
                break;
            case MINUS:
                match(TokenType.MINUS);
                answer = new OperationNode(TokenType.MINUS);
                break;
            case OR:
                match(TokenType.OR);
                answer = new OperationNode(TokenType.OR);
                break;
            default:
                error("Addop");
                break;
        }
        return answer;
    }
    
    /**
     * Executes the rule for the mulop non-terminal symbol in
     * the expression grammar.
     * @return an OperationNode with a mulop operation type
     */
    public OperationNode mulop()
    {   
        OperationNode answer = null;
        switch(lookahead.getType())
        {
            case MULTIPLY:
                match(TokenType.MULTIPLY);
                answer = new OperationNode(TokenType.MULTIPLY);
                break;
            case DIVIDE:
                match(TokenType.DIVIDE);
                answer = new OperationNode(TokenType.DIVIDE);
                break;
            case MOD:
                match(TokenType.MOD);
                answer = new OperationNode(TokenType.MOD);
                break;
            case AND:
                match(TokenType.AND);
                answer = new OperationNode(TokenType.AND);
                break;
            default:
                error("Mulop");
                break;
        }
        return answer;
    }
    
    public OperationNode relop()
    {
        OperationNode answer = null;
        switch(lookahead.getType())
        {
            case EQUAL2:
                match(TokenType.EQUAL2);
                answer = new OperationNode(TokenType.EQUAL2);
                break;
            case NOTEQUAL:
                match(TokenType.NOTEQUAL);
                answer = new OperationNode(TokenType.NOTEQUAL);
                break;
            case LESSTHAN:
                match(TokenType.LESSTHAN);
                answer = new OperationNode(TokenType.LESSTHAN);
                break;
            case LESSEQUAL:
                match(TokenType.LESSEQUAL);
                answer = new OperationNode(TokenType.LESSEQUAL);
                break;
            case GREATEQUAL:
                match(TokenType.GREATEQUAL);
                answer = new OperationNode(TokenType.GREATEQUAL);
                break;
            case GREATTHAN:
                match(TokenType.GREATTHAN);
                answer = new OperationNode(TokenType.GREATTHAN);
                break;
            default:
                error("Relop");
                break;
        }
        return answer;
    }
    
    public boolean isAtEoF(Token token)
    {
        boolean answer = false;
        
        if(token.getLexeme().equals("End of File") && token.getType() == null)
        {
            answer = true;
        }
        return answer;
    }
    
    /**
     * Matches the expected token.
     * If the current token in the input stream from the scanner
     * matches the token that is expected, the current token is
     * consumed and the scanner will move on to the next token
     * in the input.
     * The null at the end of the file returned by the
     * scanner is replaced with a fake token containing no
     * type.
     * @param expected the expected token type.
     */
    
    public void match( TokenType expected)
    {
        //System.out.println("match( " + expected + " )");
        if(this.lookahead.getType() == expected)
        {
            try
            {
                this.lookahead = scanner.next();
                if (this.lookahead == null)
                {
                    this.lookahead = new Token( "End of File", null);
                }
            }
            catch (IOException ex)
            {
                error( "Scanner exception");
            }
        }
        else
        {
            error("trying to Match " + expected + ", but found " + 
                    this.lookahead.getType() + " instead.");
        }
    }
    
    /**
     * Errors out of the Recognizer
     * Prints an error message and then exits the program
     * @param message The error message to print
     */
    public void error( String message)
    {
        throw new RuntimeException( "Error " + message + " at line " + this.scanner.getLine() + " column " + this.scanner.getColumn());
    }
}
