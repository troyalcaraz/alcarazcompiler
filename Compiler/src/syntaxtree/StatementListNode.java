/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

import java.util.ArrayList;

/**
 *
 * @author troyalcaraz
 */
public class StatementListNode extends SyntaxTreeNode {
    
    private ArrayList<StatementNode> statements = new ArrayList<StatementNode>();
    
    public void addStatement( StatementNode aStatement) {
        statements.add(aStatement);
    }
    
    public ArrayList<StatementNode> getStatements() {
        return statements;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "StatementList\n";
        for( StatementNode aStatement : statements) {
            answer += aStatement.indentedToString( level + 1);
        }
        return answer;
    }
}
