/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

/**
 *
 * @author troyalcaraz
 */
public class ReturnStatementNode extends StatementNode {
    
    private ExpressionNode returnExp;
    
    public ExpressionNode getReturnExp() { return this.returnExp;}
    
    public void setReturnExp(ExpressionNode ex) { this.returnExp = ex;}
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "ReturnStatement\n";
        answer += this.returnExp.indentedToString(level + 1);
        return answer;
    }
}
