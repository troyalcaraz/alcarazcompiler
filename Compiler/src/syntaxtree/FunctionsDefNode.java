
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a collection of function definitions.
 * @author Erik Steinmetz
 */
public class FunctionsDefNode extends SyntaxTreeNode {
    
    private ArrayList<FunctionDefNode> definitions = new ArrayList<FunctionDefNode>();
    
    public void addFunctionDefinition( FunctionDefNode aFunction) {
        definitions.add( aFunction);
    }
    
    public ArrayList<FunctionDefNode> getFunctions() {
        return definitions;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "FunctionDefinitions\n";
        for( FunctionDefNode functionDef : definitions) {
            answer += functionDef.indentedToString( level + 1);
        }
        return answer;
    }
    
}
