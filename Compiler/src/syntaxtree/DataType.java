
package syntaxtree;

/**
 * Represents an almost C data type.
 * @author Troy Alcaraz
 */
public enum DataType {
    VOID,
    INT,
    FLOAT,
}
