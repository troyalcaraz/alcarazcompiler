/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

import java.util.ArrayList;

/**
 *
 * @author Troy Alcaraz
 */
public class IdentifierListNode extends SyntaxTreeNode {
    
    private ArrayList<VariableNode> ids = new ArrayList<VariableNode>();
    
    public void addID( VariableNode aVariable) {
        ids.add(aVariable);
    }
    
    public ArrayList<VariableNode> getIDs() {
        return ids;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "IdentifierList\n";
        for( VariableNode var : ids) {
            answer += var.indentedToString( level + 1);
        }
        return answer;
    }
}
