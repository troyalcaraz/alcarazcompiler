
package syntaxtree;

import java.util.ArrayList;

/**
 * Represents a compound statement in Almost C.
 * A compound statement is a set of variable declarations
 * along with a block of zero or more
 * statements to be run sequentially.
 * @author ErikSteinmetz
 */
public class CompoundStatementNode extends StatementNode {
    
    private DeclarationsNode declarations;
    
    private StatementListNode statements = new StatementListNode();
    
    public DeclarationsNode getDeclarations() {
        return declarations;
    }

    public void setVariables(DeclarationsNode variables) {
        this.declarations = variables;
    }

    /**
     * Adds a statement to this compound statement.
     * @param state The statement to add to this compound statement.
     */
    public void addStatement( StatementNode state) {
        this.statements.addStatement(state);
    }
    
    public StatementListNode getStatements() {
        return statements;
    }
    
    /**
     * Creates a String representation of this compound statement node 
     * and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Compound Statement\n";
        answer += declarations.indentedToString(level + 1);
        for( StatementNode state : statements.getStatements()) {
            answer += state.indentedToString( level + 1);
        }
        return answer;
    }
}
