package syntaxtree;

/**
 * TokenType is a public enum containing all of the different types commonly
 * found in almost C code
 * 
 * @author Troy Alcaraz
 */

public enum TokenType
{
	LETTER,
	NUMBER,
	PLUS,
	MINUS,
	MULTIPLY,
	DIVIDE,
        MOD,
        EQUAL,
        EQUAL2,
	LBRAKET,
	RBRAKET,
	LPER,
	RPER,
	LCURLY,
	RCURLY,
	LESSTHAN,
	GREATTHAN,
	LESSEQUAL,
	GREATEQUAL,
	NOTEQUAL,
        NOT,
        COMMA,
	AND,
	OR,
	SEMICOLEN,
	IDENTIFIER,
	CHAR,
	INT,
	FLOAT,
	IF,
	ELSE,
	WHILE,
	PRINT,
	READ,
	RETURN,
	FUNC,
	PROGRAM,
	END,
        VOID,
        THEN,
        DO,
        WRITE,
        MAIN,
}