/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

import java.util.ArrayList;

/**
 *
 * @author troyalcaraz
 */
public class ExpressionListNode extends SyntaxTreeNode {
    
    private ArrayList<ExpressionNode> expressions = new ArrayList<ExpressionNode>();
    
    public void addExpression( ExpressionNode anExpression) {
        expressions.add(anExpression);
    }
    
    public ArrayList<ExpressionNode> getExpressions() {
        return expressions;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "ExpressionList\n";
        for( ExpressionNode expList : expressions) {
            answer += expList.indentedToString( level + 1);
        }
        return answer;
    }
}
