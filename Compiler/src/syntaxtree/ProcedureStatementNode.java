/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

/**
 *
 * @author troyalcaraz
 */
public class ProcedureStatementNode extends StatementNode {
    
    private String procedureId;
    
    private ExpressionListNode parameters = new ExpressionListNode();

    public String getProcedureID() {
        return procedureId;
    }

    public void setProcedureID(String anID) {
        this.procedureId = anID;
    }
    
        /**
     * Adds an Expression List to this procedure statement.
     * @param expList The list of expressions to add to this procedure statement.
     */
    public void setParamenter( ExpressionListNode expList) {
        this.parameters = expList;
    }
    
    public ExpressionListNode getParameters() {
        return parameters;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "ProcedureStatement\n";
        for( ExpressionNode anExp : parameters.getExpressions()) {
            answer += anExp.indentedToString(level + 1);
        }
        return answer;
    }
}
