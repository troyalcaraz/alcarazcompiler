/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

/**
 *
 * @author troyalcaraz
 */
public class ReadStatementNode extends StatementNode {
    
    private String readID;
    
    public String getID() { return this.readID;}
    
    public void setID(String s) { this.readID = s;}
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "ReadStatement\n";
        return answer;
    }
}
