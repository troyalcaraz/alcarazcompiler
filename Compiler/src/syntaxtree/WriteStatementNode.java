/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

/**
 *
 * @author troyalcaraz
 */
public class WriteStatementNode extends StatementNode {
    
    private ExpressionNode writeExp;
    
    public ExpressionNode getWriteExp() { return this.writeExp;}
    
    public void setWriteExp(ExpressionNode exp) { this.writeExp = exp;}
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "WriteStatement\n";
        answer += this.writeExp.indentedToString(level + 1);
        return answer;
    }
}
