/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

import java.util.ArrayList;

/**
 *
 * @author troyalcaraz
 */
public class FunctionsDecNode extends SyntaxTreeNode {
    
    private ArrayList<FunctionDecNode> declarations = new ArrayList<FunctionDecNode>();
    
    public void addFunctionDeclaration( FunctionDecNode aFunction) {
        declarations.add( aFunction);
    }
    
    public ArrayList<FunctionDecNode> getFunctions() {
        return declarations;
    }
    
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "FunctionDeclations\n";
        for( FunctionDecNode functionDec : declarations) {
            answer += functionDec.indentedToString( level + 1);
        }
        return answer;
    }
}
