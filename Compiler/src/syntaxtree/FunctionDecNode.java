/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syntaxtree;

import java.util.ArrayList;

/**
 *
 * @author troyalcaraz
 */
public class FunctionDecNode extends SyntaxTreeNode {
    
    private String name;
    private DataType returnType;
    private ArrayList<VariableNode> parameters = new ArrayList();
    
    /**
     * Creates a function declaration node with the given name.
     * @param name The name of this function.
     */
    public FunctionDecNode( String name) {
        this.name = name;
    }
    
    public String getName() { return this.name;}
    
    public DataType getReturnType() { return this.returnType;}
    
    public void setReturnType( DataType type) { this.returnType = type;}
    
    /**
     * Adds a parameter to this declaration.
     * @param aParameter The variable node to add to this declaration.
     */
    public void addParameter( VariableNode aParameter) {
        parameters.add( aParameter);
    }
    
    public ArrayList<VariableNode> getParameters() {
        return parameters;
    }
    
    /**
     * Creates a String representation of this function definition node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Function: " + this.name + " returns " + this.returnType + "\n";
        for( VariableNode parameter : parameters) {
            answer += parameter.indentedToString( level + 1);
        }
        return answer;
    }
}
