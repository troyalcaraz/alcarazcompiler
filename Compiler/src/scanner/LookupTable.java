package scanner;

import syntaxtree.TokenType;
import java.util.HashMap;

/**
 * LookupTable is a HashMap that stores all of the keywords
 * in "Micro C".
 * We do not use the LookupTable directly. Instead our scanner
 * uses it to determine if an identifier is a keyword or not.
 * 
 * @author Troy Alcaraz
 */
public class LookupTable extends HashMap
{
	TokenType type;

	public LookupTable() 
	{
            this.put("char",    TokenType.CHAR);
            this.put("int",     TokenType.INT);
            this.put("float",   TokenType.FLOAT);
            this.put("if",      TokenType.IF);
            this.put("else",    TokenType.ELSE);
            this.put("while",   TokenType.WHILE);
            this.put("print",   TokenType.PRINT);
            this.put("write",   TokenType.WRITE);
            this.put("read",    TokenType.READ);
            this.put("return",  TokenType.RETURN);
            this.put("func",    TokenType.FUNC);
            this.put("program", TokenType.PROGRAM);
            this.put("end",     TokenType.END);
            this.put("void",    TokenType.VOID);
            this.put("main",    TokenType.MAIN);
            this.put("[",       TokenType.LBRAKET);
            this.put("]",       TokenType.RBRAKET);
	}
}