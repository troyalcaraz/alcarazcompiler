package scanner;

// ScannerMain.java

import java.io.StringReader;

/**
 * Tests the Scanner.
 */
public class ScannerMain
{
    public static void main(String[] args) throws Exception
    {
        String testOne = "34 if + ; && || % == = [ ] () {} ! , main write";
        Scanner tScanner = new Scanner( new StringReader( testOne));
        Token result = null;
        try {
            result = tScanner.next();
        } catch (Exception e) {}
        while( result != null) {
            System.out.println("next() returned " + result);
            try {
                result = tScanner.next();
            } catch (Exception e) {}
        }
        System.out.println("Done!");
    }

}