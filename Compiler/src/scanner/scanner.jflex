package scanner;

// scanner.jflex
// Troy Alcaraz
// Scanner code with Tokens

/**
 * Scanner finds the lexemes for code commonly found in java programs
 */
%%

%public 
%class Scanner
%function next
%type Token
%line
%column

%{

/**
 * Gets the line number of the most recent lexeme.
 * @return The current line number.
 */
public int getLine() { return yyline;}

/**
 * Gets the column number of the most recent lexeme.
 * This is the number of chars since the most recent newline char.
 * @return The current column number.
 */
public int getColumn() { return yycolumn;}

/** The lookup table of token types for symbols. */
private LookupTable myTable = new LookupTable();
%}

%eofval{
    return null;
%eofval}

letter     = [a-zA-z]
identifier = {letter}+ | {letter}+{number}*

digits     = [1-9][0-9]*|0
optionalFraction = {digits}\.{digits} | 
optionalExponent = \E\( {digits} \)

number	   = {digits} | {optionalFraction}

operator   = {relop}|{addop}|{mulop}|{assignop}|"!"|","
relop	   = [\<\>]|[\!\<\>\=][\=]
addop      = [\+\-]|[\|][\|]
mulop      = [\*/\%]|[\&][\&]
assignop   = [\=]

containers = "[" | "]" | "{" | "}" | "(" | ")"
semicolen  = [\;]

whitespace = [ \n\t]+

comment = {TraditionalComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

%%

/*
{letter}	 {
			   //System.out.println("Found a letter: " + yytext());
			   Token t = new Token();
			   t.lexeme = yytext();
			   t.type = TokenType.LETTER;	
			   return( t);
			 }
*/
			 
{identifier} {
			   { if (myTable.containsKey(yytext()))
			     {
                   return new Token(yytext(), (TokenType) myTable.get(yytext()));
			     }
			     else
			     {
			       Token t = new Token();
			       t.lexeme = yytext();
			       t.type = TokenType.IDENTIFIER;
			       return( t);
			     }
			   }
			 }

{number}     {
               Token t = new Token();
               t.lexeme = yytext();
               t.type = TokenType.NUMBER;
               return( t);
             }

{operator}   {
               Token t = new Token();
               t.lexeme = yytext();
               switch( yytext()) {
                   case "+"  : t.type = TokenType.PLUS;       break;
                   case "-"  : t.type = TokenType.MINUS;      break;
                   case "*"  : t.type = TokenType.MULTIPLY;   break;
                   case "/"  : t.type = TokenType.DIVIDE;     break;
                   case "%"  : t.type = TokenType.MOD;		  break;
                   case "="  : t.type = TokenType.EQUAL;      break;
                   case "<"  : t.type = TokenType.LESSTHAN;   break;
                   case ">"  : t.type = TokenType.GREATTHAN;  break;
                   case "==" : t.type = TokenType.EQUAL2;	  break;
                   case "<=" : t.type = TokenType.LESSEQUAL;  break;
                   case ">=" : t.type = TokenType.GREATEQUAL; break;
                   case "!=" : t.type = TokenType.NOTEQUAL;   break;
                   case "&&" : t.type = TokenType.AND;        break;
                   case "||" : t.type = TokenType.OR;         break;
                   case "!"  : t.type = TokenType.NOT;        break;
                   case ","  : t.type = TokenType.COMMA;      break;
               }
               return( t);               
             }
             
{containers} {
			   Token t = new Token();
			   t.lexeme = yytext();
			   switch( yytext()) {
			   	   case "["  : t.type = TokenType.LBRAKET;    break;
                   case "]"  : t.type = TokenType.RBRAKET;    break;
                   case "("  : t.type = TokenType.LPER;       break;
                   case ")"  : t.type = TokenType.RPER;       break;
                   case "{"  : t.type = TokenType.LCURLY;     break;
                   case "}"  : t.type = TokenType.RCURLY;     break;
			   }
			   return ( t);
			 }
			 
{semicolen}  {
		Token t = new Token();
		t.lexeme = yytext();
		t.type = TokenType.SEMICOLEN;
		return ( t);
	     }

{whitespace} { 
               // Please ignore whitespace
             }
             
{comment}    {
	       // Please ignore comments
	     }
             
.            {
               System.out.println("Found an illegal char: " + yytext());
             }