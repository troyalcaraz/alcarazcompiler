package scanner;

import syntaxtree.TokenType;


/**
 * Token identifies what type a lexeme is and whether two tokens are equal
 * 
 * @author Troy Alcaraz
 */
public class Token
{
     String lexeme;
     TokenType type;
     
     /**
      * Creates an instance of a Token
      * @param s a String
      * @param t a TokenType
      */
     public Token(String s, TokenType t) {
         lexeme = s;
         type = t;
     }
     
     /**
      * Creates an empty Token you can manuely insert a
      * lexeme and TokenType
      */
     public Token() {
     }
     
     // Getters and Setters
     
     public String getLexeme() { return this.lexeme;}
     public TokenType getType() { return this.type;}
     
     /**
      * Prints a lexeme and its TokenType
      * @return a String identifying the type of a lexeme
      */
     @Override
     public String toString() {
         return lexeme + " : " + type;
     }
     /**
      * Tests to see if two lexemes are the same
      * @param o another lexeme
      * @return true is two lexemes are the same, false otherwise
      */
     @Override
     public boolean equals(Object o) {
     	if ( this.hashCode() == o.hashCode()) {
            return true;
        }
        return false;
     }
     
     @Override
     public int hashCode() {
         return type.hashCode() + lexeme.hashCode();
     }
}