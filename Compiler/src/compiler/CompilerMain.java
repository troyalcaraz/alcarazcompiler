
package compiler;

import java.util.Formatter;
import parser.Parser;
import parser.Recognizer;
import analyzer.SemanticAnalyzer;
import syntaxtree.*;
import codegen.CodeGeneration;
/**
 *
 * @author Troy Alcaraz
 */
public class CompilerMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String fileName = fileName = args[0];
//        String fileName = "money.ac";
        
        String fileString = fileName.substring(0, fileName.indexOf('.', 0));
        
        Recognizer r = new Recognizer(fileName, true);
        Parser p     = new Parser(fileName, true);
        r.program();
        ProgramNode pn = p.program();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn, p.symTable);
        CodeGeneration generator = new CodeGeneration();
        Formatter f1 = null;
        Formatter f2 = null;
        Formatter f3 = null;
        
        try {
            f1 = new Formatter(fileString + "Symbols.txt");
            f2 = new Formatter(fileString + "SyntaxTree.txt");
            f3 = new Formatter(fileString + ".asm");
        } catch (Exception e) {
            System.out.println("Something went wrong");
        }
        
        sa.checkDeclarations();
        sa.assignDataType();
        sa.checkAssignmentTypes();

        String s = r.symTable.toString();
        String tree = pn.indentedToString(0);
        String code = generator.writeCodeForRoot(pn);
        
        f1.format("%s", s);
        f2.format("%s", tree);
        f3.format("%s", code);
        f1.close();
        f2.close();
        f3.close();
        
//        System.out.println(tree);
//        System.out.println("Can write assembly: " + sa.getCanWriteAssembly());
//        System.out.println(code);
//        System.out.println(fileString);
    }
}
