
package codegen;

import syntaxtree.*;

/**
 * This class will create code for an Equation tree.
 * @author Erik Steinmetz
 * @author Troy Alcaraz
 */
public class CodeGeneration {
    
    private int currentTRegister = 0;
    private int currentFRegister = 0;
    private int ifNum = 0;
    private int whileNum = 0;
    private int branchNum = 0;
    
    /**
     * Starts the code from the root node by writing the outline of the
     * assembly code, and telling the root node to write its answer into $s0.
     *
     * @param root The root node of the program to be written
     * @return A String of the assembly code.
     */
    public String writeCodeForRoot( ProgramNode root) 
    {
        StringBuilder code = new StringBuilder();
        code.append( ".data\n");
        String decNodeCode = "";
        DeclarationsNode decNode = root.getMain().getDeclarations();
        decNodeCode = writeCode(decNode);
        code.append(decNodeCode + "\n\n");
        code.append( ".text\n");
        code.append( "main:\n");
        
        String compNodeCode = "";
        int tempTRegValue = this.currentTRegister;
        int tempFRegValue = this.currentFRegister;
        for( StatementNode statement : root.getMain().getStatements().getStatements()) {
            compNodeCode += writeCode(statement);
        }
        this.currentTRegister = tempTRegValue;
        this.currentFRegister = tempFRegValue;
        code.append( compNodeCode);
        code.append( "li     $v0,   10\n");
        code.append( "syscall\n");
        return( code.toString());
    }
    
    /**
     * Writes code for the given node.
     * This generic write code takes any ExpressionNode, and then
     * recasts according to subclass type for dispatching.
     * @param node The node for which to write code.
     * @param reg The register in which to put the result.
     * @return The code which executes the expression node
     */
    private String writeCode( ExpressionNode node, String reg) {
        String nodeCode = "";
        if( node instanceof OperationNode) {
            nodeCode = writeCode( (OperationNode)node, reg);
        }
        else if( node instanceof ValueNode) {
            nodeCode = writeCode( (ValueNode)node, reg);
        }
        else if( node instanceof VariableNode) {
            nodeCode = writeCode( (VariableNode)node, reg);
        }
        return( nodeCode);
    }
    
    /**
     * Writes code for an operations node.
     * The code is written by gathering the child nodes' answers into
     * a pair of registers, and then executing the op on those registers,
     * placing the result in the given result register.
     * @param opNode The operation node to perform.
     * @param resultRegister The register in which to put the result.
     * @return The code which executes this operation.
     */
    public String writeCode( OperationNode opNode, String resultRegister)
    {
        String code = "";
        ExpressionNode left = opNode.getLeft();
        if(opNode.getLeft().getType() == DataType.INT) {
            
            String leftRegister = "$t" + currentTRegister++;
            code += writeCode( left, leftRegister);
            ExpressionNode right = opNode.getRight();
            String rightRegister = "$t" + currentTRegister++;
            code += writeCode( right, rightRegister);
            TokenType kindOfOp = opNode.getOperation();

            switch(kindOfOp) {
                case PLUS:
                    // add resultregister, left, right 
                    code += "add    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case MINUS:
                    // sub resultregister, left, right 
                    code += "sub    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case MULTIPLY:
                    // multiply left, right
                    code += "mult   " + leftRegister + ",   " + rightRegister + "\n";
                    // move result from low register to resultRegister
                    code += "mflo   " + resultRegister + "\n";
                    break;
                case DIVIDE:
                    // divide left, right
                    code += "div    " + leftRegister + ",   " + rightRegister + "\n";
                    // move result from low register to resultRegister
                    code += "mflo   " + resultRegister + "\n";
                    break;
                case MOD:
                    // divide left, right
                    code += "div    " + leftRegister + ",   " + rightRegister + "\n";
                    // move remainder from hi register to resultRegister
                    code += "mfhi   " + resultRegister + "\n";
                    break;
                case LESSTHAN:
                    String ltLabel = "lesser" + branchNum;
                    String ltExit = "lessExit" + branchNum++;
                    code += "blt    " + leftRegister + ",   " + rightRegister + ",   " + ltLabel + "\n" +
                            "li     " + resultRegister + ",   " + "0\n" +
                            "j      " + ltExit + "\n" +
                            ltLabel + ":\n" +
                            "li     " + resultRegister + ",   " + "1\n" +
                            ltExit + ":\n";
                    break;

                case GREATTHAN:
                    String gtLabel = "greater" + branchNum;
                    String gtExit = "greatExit" + branchNum++;
                    code += "bgt    " + leftRegister + ",   " + rightRegister + ",   " + gtLabel + "\n" +
                            "li     " + resultRegister + ",   " + "0\n" +
                            "j      " + gtExit + "\n" +
                            gtLabel + ":\n" +
                            "li     " + resultRegister + ",   " + "1\n" +
                            gtExit + ":\n";
                    break;

                case LESSEQUAL:
                    String lteLabel = "lessEqual" + branchNum;
                    String lteExit = "lessEqaulExit" + branchNum++;
                    code += "ble    " + leftRegister + ",   " + rightRegister + ",   " + lteLabel + "\n" +
                            "li     " + resultRegister + ",   " + "0\n" +
                            "j      " + lteExit + "\n" +
                            lteLabel + ":\n" +
                            "li     " + resultRegister + ",   " + "1\n" +
                            lteExit + ":\n";
                    break;

                case GREATEQUAL:
                    String gteLabel = "greatEqual" + branchNum;
                    String gteExit = "greatEqualExit" + branchNum++;
                    code += "bge    " + leftRegister + ",   " + rightRegister + ",   " + gteLabel + "\n" +
                            "li     " + resultRegister + ",   " + "0\n" +
                            "j      " + gteExit + "\n" +
                            gteLabel + ":\n" +
                            "li     " + resultRegister + ",   " + "1\n" +
                            gteExit + ":\n";
                    break;

                case NOTEQUAL:
                    String neLabel = "notEqual" + branchNum;
                    String neExit = "notEqualExit" + branchNum++;
                    code += "bne    " + leftRegister + ",   " + rightRegister + ",   " + neLabel + "\n" +
                            "li     " + resultRegister + ",   " + "0\n" +
                            "j      " + neExit + "\n" +
                            "label" + ":\n" +
                            "li     " + resultRegister + ",   " + "1\n" +
                            neExit + ":\n";
                    break;

                case EQUAL2:
                    String eLabel = "equal" + branchNum;
                    String eExit = "equalExit" + branchNum++;
                    code += "beq    " + leftRegister + ",   " + rightRegister + ",   " + eLabel + "\n" +
                            "li     " + resultRegister + ",   " + "0\n" +
                            "j      " + eExit + "\n" +
                            "label" + ":\n" +
                            "li     " + resultRegister + ",   " + "1\n" +
                            eExit + ":\n";
                    break;

                case AND:
                    code += "and    " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case OR:
                    code += "or     " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                default:
                    break;
            }
        }
        if(opNode.getLeft().getType() == DataType.FLOAT) {
            //TODO for Float extra
//            int leftNum = currentFRegister+2;
//            int rightNum = currentFRegister+2;
            String leftRegister = "$f" + currentFRegister;
            currentFRegister += 2;
            code += writeCode( left, leftRegister);
            ExpressionNode right = opNode.getRight();
            String rightRegister = "$f" + currentFRegister;
            currentFRegister += 2;
            code += writeCode( right, rightRegister);
            TokenType kindOfOp = opNode.getOperation();
            
            switch(kindOfOp) {
                case PLUS:
                    // add resultregister, left, right 
                    code += "add.s  " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case MINUS:
                    // subtratcts resultregister, left, right 
                    code += "sub.s  " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case MULTIPLY:
                    // multiply resultregister, left, right 
                    code += "mul.s  " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case DIVIDE:
                    // divide resultregister, left, right 
                    code += "dis.s  " + resultRegister + ",   " + leftRegister +
                        ",   " + rightRegister + "\n";
                    break;
                case MOD:
                    //TODO
                    break;
                case LESSTHAN:
                    String ltLabel = "floatLesser" + branchNum;
                    String ltExit = "floatLessExit" + branchNum++;
                    code += "c.lt.s " + leftRegister + ",   " + rightRegister + "\n" +
                            "bc1t   " + ltLabel + "\n" +
                            "li     " + "$s0" + ",   " + "0\n" +
                            "j      " + ltExit + "\n" +
                            ltLabel + ":\n" +
                            "li     " + "$s0" + ",   " + "1\n" +
                            ltExit + ":\n";
                    break;
                case GREATTHAN:
                    String gtLabel = "floatGreater" + branchNum;
                    String gtExit = "floatGreatExit" + branchNum++;
                    code += "c.gt.s " + leftRegister + ",   " + rightRegister + "\n" +
                            "bc1t   " + gtLabel + "\n" +
                            "li     " + "$s0" + ",   " + "0\n" +
                            "j      " + gtExit + "\n" +
                            gtLabel + ":\n" +
                            "li     " + "$s0" + ",   " + "1\n" +
                            gtExit + ":\n";
                    break;
                case LESSEQUAL:
                    String lteLabel = "floatLessEqual" + branchNum;
                    String lteExit = "floatLessEqualExit" + branchNum++;
                    code += "c.le.s " + leftRegister + ",   " + rightRegister + "\n" +
                            "bc1t   " + lteLabel + "\n" +
                            "li     " + "$s0" + ",   " + "0\n" +
                            "j      " + lteExit + "\n" +
                            lteLabel + ":\n" +
                            "li     " + "$s0" + ",   " + "1\n" +
                            lteExit + ":\n";
                    break;
                case GREATEQUAL:
                    String gteLabel = "floatGreatEqual" + branchNum;
                    String gteExit = "floatGreatEqualExit" + branchNum++;
                    code += "c.ge.s " + leftRegister + ",   " + rightRegister + "\n" +
                            "bc1t   " + gteLabel + "\n" +
                            "li     " + "$s0" + ",   " + "0\n" +
                            "j      " + gteExit + "\n" +
                            gteLabel + ":\n" +
                            "li     " + "$s0" + ",   " + "1\n" +
                            gteExit + ":\n";
                    break;
                case NOTEQUAL:
                    String neLabel = "floatNotEqual" + branchNum;
                    String neExit = "floatNotEqualExit" + branchNum++;
                    code += "c.ne.s " + leftRegister + ",   " + rightRegister + "\n" +
                            "bc1t   " + neLabel + "\n" +
                            "li     " + "$s0" + ",   " + "0\n" +
                            "j      " + neExit + "\n" +
                            neLabel + ":\n" +
                            "li     " + "$s0" + ",   " + "1\n" +
                            neExit + ":\n";
                    break;
                case EQUAL2:
                    String eLabel = "floatEqual" + branchNum;
                    String eExit = "floatEqualExit" + branchNum++;
                    code += "c.eq.s " + leftRegister + ",   " + rightRegister + "\n" +
                            "bc1t   " + eLabel + "\n" +
                            "li     " + "$s0" + ",   " + "0\n" +
                            "j      " + eExit + "\n" +
                            eLabel + ":\n" +
                            "li     " + "$s0" + ",   " + "1\n" +
                            eExit + ":\n";
                    break;
                case AND:
                    //TODO
                    break;
                case OR:
                    //TODO
                    break;
                default:
                    break;
            }
        }
        
        this.currentTRegister -= 2;
        this.currentFRegister -= 4;
        return( code);
    }
    
    /**
     * Writes code for a statement node.
     * The code is written by gathering the child nodes' answers into
     * a pair of registers, and then executing the statement and its
     * expressions on those registers, placing the result in the given 
     * result register.
     * @param statement the statement to perform
     * @return the code that executes the statement
     */
    private String writeCode( StatementNode statement) {
        String code = "";
        
        if (statement instanceof AssignmentStatementNode) {
            AssignmentStatementNode asn = (AssignmentStatementNode) statement;
            code += writeCode(asn);
        } 
        else if (statement instanceof CompoundStatementNode) {
            CompoundStatementNode csn = (CompoundStatementNode) statement;
            DeclarationsNode dn = csn.getDeclarations();
            
            code += writeCode(dn);
            
            for(StatementNode aStatement : csn.getStatements().getStatements()) {
                code += writeCode(aStatement);
            }
        } 
        else if (statement instanceof IfStatementNode) {
            IfStatementNode isn = (IfStatementNode) statement;
            code += writeCode(isn);
        } 
        else if (statement instanceof ProcedureStatementNode) {
            ProcedureStatementNode psn = (ProcedureStatementNode) statement;
            code += writeCode(psn);
            
        } 
        else if (statement instanceof ReturnStatementNode) {
            ReturnStatementNode rsn = (ReturnStatementNode) statement;
            code += writeCode(rsn);
        } 
        else if (statement instanceof WhileStatementNode) {
            WhileStatementNode whsn = (WhileStatementNode) statement;
            code += writeCode(whsn);
        } 
        else if (statement instanceof WriteStatementNode) {
            WriteStatementNode wrsn = (WriteStatementNode) statement;
            code += writeCode(wrsn);
        }
        return code;
    }
    
    /**
     * Writes the code for an assignment statement node.
     * The code is written by executing a store word with the variable into
     * the destination register.
     * Writes code that looks like  sw $reg, variable
     * @param asn The node containing the assignment
     * @return The code which executes the assignment node.
     */
    private String writeCode( AssignmentStatementNode asn) {
        String code = "";
        String expressionCode = "";
        if(asn.getExpression().getType() == DataType.INT) {
            expressionCode = writeCode(asn.getExpression(), "$s0");
        }
        else if(asn.getExpression().getType() == DataType.FLOAT) {
            expressionCode = writeCode(asn.getExpression(), "$f30");
        }
        
        String varName = asn.getLvalue().getName();
        if(asn.getLvalue().getType() == DataType.INT) {
            code = expressionCode + "sw     $s0,   " + varName + "\n";
        }
        else if(asn.getLvalue().getType() == DataType.FLOAT) {
            code = expressionCode + "swc1   $f30,   " + varName + "\n";
        }
        return code;
    }
    
    /**
     * Writes the code for an if statement node.
     * The code is written executing a branch if equal, and jump to control the 
     * flow of the code.
     * @param isn The node containing the if statement
     * @return The code which executes the if statement node.
     */
    private String writeCode( IfStatementNode isn) {
        
        String code = "";
        String elseCodeNum = "elseCode" + ifNum;
        String endIfNum = "endIf" + ifNum++;
        if(isn.getTest().getType() == DataType.INT) {
            String testCode = writeCode(isn.getTest(), "$s0");
            String thenCode = writeCode(isn.getThenStatement());
            String elseCode = writeCode(isn.getElseStatement());
            
            code = testCode + 
                   "beq    $s0,   $zero, " + elseCodeNum + "\n" +
                   thenCode +
                   "j " + endIfNum + "\n" +
                   elseCodeNum + ":\n" +
                   elseCode +
                   endIfNum + ":\n";
        }
        else if(isn.getTest().getType() == DataType.FLOAT) {
            //TODO
            String testCode = writeCode(isn.getTest(), "$s0");
            String thenCode = writeCode(isn.getThenStatement());
            String elseCode = writeCode(isn.getElseStatement());
            
            code = testCode +
                    "beq   $s0,   $zero, " + elseCodeNum + "\n" +
                    thenCode +
                    "j " + endIfNum + "\n" +
                    elseCodeNum + ":\n" +
                    elseCode +
                    endIfNum + ":\n";
        }
        
        return code;
    }
    
    /**
     * TODO for Function Extra
     * Writes the code for a procedure statement node.
     * The code is written executing things.
     * @param psn The node containing the procedure statement
     * @return The code which executes the procedure statement node.
    
    private String writeCode( ProcedureStatementNode psn) {
        String name = psn.getProcedureID();
        String expressionsCode = "";
        for( ExpressionNode exp : psn.getParameters().getExpressions()) {
            expressionsCode += writeCode(exp, "$s0");
        }
        
        return name + ":\n" +
               expressionsCode;
    }
    */
    
    /**
     * TODO for Function Extra
     * Writes the code for a return statement node.
     * The code is written executing things.
     * @param rsn The node containing the return statement
     * @return The code which executes the return statement node.
    
    private String writeCode( ReturnStatementNode rsn) {
        String expressionCode = writeCode(rsn.getReturnExp(), "$a0");
        
        return expressionCode +
               "li     $v0,   1\n" +
               "syscall\n";
    }
    */
    
    /**
     * TODO
     * Writes the code for a while statement node.
     * The code is written executing a branch if equal and jump to control the 
     * flow of the code.
     * @param wsn The node containing the while statement
     * @return The code which executes the while statement node.
     */
    private String writeCode( WhileStatementNode wsn) {
        
        String code = "";
        
        if(wsn.getTest().getType() == DataType.INT) {
            String testCode = writeCode(wsn.getTest(), "$s0");
            String whileCode = writeCode(wsn.getDoStatement());
            code = "while" + whileNum + ":\n" +
                   testCode +
                   "beq    $s0,   $zero, exit" + whileNum + "\n" +
                   whileCode +
                   "j      while" + whileNum + "\n" +
                   "exit" + whileNum++ + ":\n";
        }
        else if (wsn.getTest().getType() == DataType.FLOAT) {
            //TODO
            String testCode = writeCode(wsn.getTest(), "$s0");
            String whileCode = writeCode(wsn.getDoStatement());
            code = "while" + whileNum + ":\n" +
                   testCode +
                   "beq   $s0,   $zero, exit" + whileNum + "\n" +
                   whileCode + 
                   "j      while" + whileNum + "\n" +
                   "exit" + whileNum++ + ":\n";
        }
        return code;
    }
    
    /**
     * Writes the code for a write statement node.
     * The code is written by executing and add immediate of the statement's
     * expression into the $a0 register, load immediate a 1 into the $v0 register,
     * and then a syscall to print the integer.
     * @param wsn The node containing the write statement
     * @return The code which executes the write statement.
     */
    private String writeCode( WriteStatementNode wsn) {
        
        String code = "";
            
        if (wsn.getWriteExp().getType() == DataType.INT) {
            String expressionCode = writeCode(wsn.getWriteExp(), "$s0");
            String writeIntStatementCode = "add    $a0,   $s0,   $zero\n" + 
                                           "li     $v0,   1\n" +
                                           "syscall\n";
            code = expressionCode + writeIntStatementCode;
        }
        else if (wsn.getWriteExp().getType() == DataType.FLOAT) {
            String expressionCode = writeCode(wsn.getWriteExp(), "$f30");
            String writeFloatStatementCode = "mov.s  $f12,  $f30\n" +
                                             "li     $v0,   2\n" +
                                             "syscall\n";
            
            code = expressionCode + writeFloatStatementCode;
        }
        
        return code;
    }
    
    /**
     * Writes code for a value node.
     * The code is written by executing an load immediate with the value
     * into the destination register.
     * Writes code that looks like  li $reg, value
     * @param valNode The node containing the value.
     * @param resultRegister The register in which to put the value.
     * @return The code which executes this value node.
     */
    public String writeCode( ValueNode valNode, String resultRegister)
    {
        String code = "";
        String value = valNode.getAttribute();
        if(valNode.getType() == DataType.INT) {
            code = "li     " + resultRegister + ",   " + value + "\n";
        }
        else if(valNode.getType() == DataType.FLOAT) {
            code = "li.s   " + resultRegister + ",   " + value + "\n";
        }
        return code;
    }
    
    /**
     * Writes code for a variable node.
     * The code is written by executing a load word with the variable into
     * the destination register.
     * Writes code that looks like lw $reg, variable.
     * @param varNode The node containing the variable.
     * @param resultRegister The register in which to put variable value.
     * @return The code which executes this variable node.
     */
    public String writeCode( VariableNode varNode, String resultRegister) {
        
        String code = "";
        String variable = varNode.getName();
        
        if(varNode.getType() == DataType.INT) {
            code = "lw     " + resultRegister + ",   " + variable + "\n";
        }
        else if (varNode.getType() == DataType.FLOAT) {
            code = "lwc1   " + resultRegister + ",   " + variable + "\n";
        }
        return code;
    }
    
    /**
     * Writes the code for any declared variables
     * @param decNode the node holding all the declared variables
     * @return The code which executes this declarations node
     */
    private String writeCode(DeclarationsNode decNode) {
        String code = "";
        String colen = ":";
        String myFloat = ".float";
        String word = ".word";
        String zero = "0";
        String fZero = "0.0";
        
        for( VariableNode variable : decNode.getVariables()) {
            if(variable.getType() == DataType.INT) {
                String varString = String.format("%-10s %-1s %-1s",
                    (variable.getName() + colen),
                    word,
                    zero);
            
                    code += varString + "\n";
            }
            else if(variable.getType() == DataType.FLOAT) {
                String floatString = String.format("%-10s %-1s %-1s",
                    (variable.getName() + colen),
                    myFloat,
                    fZero);
            
                    code += floatString + "\n";
            }
        }
        return code;
    }
}
