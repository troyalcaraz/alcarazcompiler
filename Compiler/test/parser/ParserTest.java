/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import syntaxtree.*;

/**
 *
 * @author troyalcaraz
 */
public class ParserTest {
    
    public ParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of program method, of class Parser.
     */
    @Test
    public void testProgram() {
        System.out.println("program");
        Parser instance = new Parser( "main() { float a, b, c, d; a = b + 15;}", false);
        ProgramNode p = new ProgramNode();
        
        String expResult = null;
        String result = instance.program().indentedToString(0);
        System.out.println(result);
        //assertEquals(expResult, result);
    }

    /**
     * Test of declarations method, of class Parser.
     */
    @Test
    public void testDeclarations() {
        System.out.println("declarations");
        Parser instance = new Parser("int one; float two;", false);
        DeclarationsNode dn = new DeclarationsNode();
        VariableNode v1 = new VariableNode("one");
        VariableNode v2 = new VariableNode("two");
        dn.addVariable(v1);
        dn.addVariable(v2);
        String expResult = dn.indentedToString(0);
        String result = instance.declarations().indentedToString(0);
        assertEquals(expResult, result);
    }

    /**
     * Test of functionDefinition method, of class Parser.
     */
    @Test
    public void testFunctionDefinition() {
        System.out.println("functionDefinition");
        Parser instance = new Parser("float gradePercentage (float a, float b, float c, float d, float f) {int student1 , student2 , student3 ; }", false);
        FunctionDefNode fdn = new FunctionDefNode("gradePercentage");
        fdn.setReturnType(DataType.FLOAT);
        VariableNode a = new VariableNode("a");
        VariableNode b = new VariableNode("b");
        VariableNode c = new VariableNode("c");
        VariableNode d = new VariableNode("d");
        VariableNode f = new VariableNode("f");
        fdn.addParameter(a);
        fdn.addParameter(b);
        fdn.addParameter(c);
        fdn.addParameter(d);
        fdn.addParameter(f);
        CompoundStatementNode csn = new CompoundStatementNode();
        DeclarationsNode dn = new DeclarationsNode();
        VariableNode s1 = new VariableNode("student1");
        VariableNode s2 = new VariableNode("student2");
        VariableNode s3 = new VariableNode("student3");
        dn.addVariable(s1);
        dn.addVariable(s2);
        dn.addVariable(s3);
        csn.setVariables(dn);
        fdn.setBody(csn);
        String expResult = fdn.indentedToString(0);
        String result = instance.functionDefinition().indentedToString(0);
        assertEquals(expResult, result);
    }

    /**
     * Test of statement method, of class Parser.
     */
    @Test
    public void testStatement() {
        System.out.println("statement");
        Parser instance = new Parser("if 24 return 69 else return 21", false);
        IfStatementNode isn = new IfStatementNode();
        ReturnStatementNode rsn1 = new ReturnStatementNode();
        ReturnStatementNode rsn2 = new ReturnStatementNode();
        ValueNode v1 = new ValueNode("24");
        ValueNode v2 = new ValueNode("69");
        ValueNode v3 = new ValueNode("21");
        isn.setTest(v1);
        isn.setThenStatement(rsn1);
        rsn1.setReturnExp(v2);
        isn.setElseStatement(rsn2);
        rsn2.setReturnExp(v3);
        String expResult = isn.indentedToString(0);
        String result = instance.statement().indentedToString(0);
        assertEquals(expResult, result);
    }

    /**
     * Test of simpleExpression method, of class Parser.
     */
    @Test
    public void testSimpleExpression() {
        System.out.println("simpleExpression");
        Parser instance = new Parser("+4 * num0 - 11", false);
        OperationNode op1 = new OperationNode(TokenType.MINUS);
        OperationNode op2 = new OperationNode(TokenType.MULTIPLY);
        ValueNode val1 = new ValueNode("4");
        VariableNode val2 = new VariableNode("num0");
        ValueNode v1 = new ValueNode("11");
        op1.setLeft(op2);
        op1.setRight(v1);
        op2.setLeft(val1);
        op2.setRight(val2);
        String expResult = op1.indentedToString(0);
        String result = instance.simpleExpression().indentedToString(0);
        assertEquals(expResult, result);
    }

    /**
     * Test of factor method, of class Parser.
     */
    @Test
    public void testFactor() {
        System.out.println("factor");
        Parser instance = new Parser("(99 > 1)", false);
        OperationNode  op = new OperationNode(TokenType.GREATTHAN);
        ValueNode v1 = new ValueNode("99");
        ValueNode v2 = new ValueNode("1");
        op.setLeft(v1);
        op.setRight(v2);
        String expResult = op.indentedToString(0);
        String result = instance.factor().indentedToString(0);
        assertEquals(expResult, result);
    }
    
}
