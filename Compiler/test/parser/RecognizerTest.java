/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import symboltable.Kind;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import scanner.Token;

/**
 *
 * @author troyalcaraz
 */
public class RecognizerTest {
    
    public RecognizerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void hTestProgram0() {
        System.out.println("Happy Program 0");
        Recognizer r = new Recognizer("main () {}", false);
        r.program();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void hTestProgram1() {
        System.out.println("Happy Program 1");
        Recognizer r = new Recognizer("int numbers (int num0, int num1, int num2); main () {}", false);
        r.program();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void hTestProgram2() {
        System.out.println("Happy Program 2");
        Recognizer r = new Recognizer("main () {float a, b, c, d;}", false);
        r.program();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void sTestProgram0() {
        System.out.println("Sad Program 0");
        Recognizer r = new Recognizer("main [ ] {}", false);
        try
        {
            r.program();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void sTestProgram1() {
        System.out.println("Sad Program 1");
        Recognizer r = new Recognizer("void main () {}", false);
        try
        {
            r.program();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of program method, of class Recognizer.
     */
    @Test
    public void sTestProgram2() {
        System.out.println("Sad Program 2");
        Recognizer r = new Recognizer("int letters (char a) main () {}", false);
        try
        {
            r.program();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void hDeclarations0() {
        System.out.println("Happy Declaration 0");
        Recognizer r = new Recognizer("void nothing;", false);
        r.declarations();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void hDeclarations1() {
        System.out.println("Happy Declaration 1");
        Recognizer r = new Recognizer("float a, b, c, d;", false);
        r.declarations();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void hDeclarations2() {
        System.out.println("Happy Declaration 2");
        Recognizer r = new Recognizer("int one; float two;", false);
        r.declarations();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void sDeclarations0() {
        System.out.println("Sad Declaration 0");
        Recognizer r = new Recognizer("long a, b, c, d;", false);

        r.declarations();
        assertFalse(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void sDeclarations1() {
        System.out.println("Sad Declaration 1");
        Recognizer r = new Recognizer("void ;", false);
        try
        {
            r.declarations();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of declarations method, of class Recognizer.
     */
    @Test
    public void sDeclarations2() {
        System.out.println("Sad Declaration 2");
        Recognizer r = new Recognizer("int a; b; c;", false);

        r.declarations();

        Token result = r.lookahead;
        assertNotNull(result.getType());
    }
    
    /**
     * Test of functionDefinition method, of class Recognizer.
     */
    @Test
    public void hFunctionDefinition0() {
        System.out.println("Happy Function Definition 0");
        Recognizer r = new Recognizer("void foo (int bar) {}", false);
        r.functionDefinition();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of functionDefinition method, of class Recognizer.
     */
    @Test
    public void hFunctionDefinition1() {
        System.out.println("Happy Function Definition 1");
        Recognizer r = new Recognizer("float gradePercentage (float a, float b, float c, float d, float f) {int student1 , student2 , student3 ; }", false);
        r.functionDefinition();
        Token result = r.lookahead;
        System.out.println("blah" + r.lookahead);
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of functionDefinition method, of class Recognizer.
     */
    @Test
    public void hFunctionDefinition2() {
        System.out.println("Happy Function Definition 2");
        Recognizer r = new Recognizer("void a () {}", false);
        r.functionDefinition();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of functionDefinition method, of class Recognizer.
     */
    @Test
    public void sFunctionDefinition0() {
        System.out.println("Sad Function Definition 0");
        Recognizer r = new Recognizer("void abc (long def) {short ghi, jkl, mno}", false);
        try
        {
            r.functionDefinition();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of functionDefinition method, of class Recognizer.
     */
    @Test
    public void sFunctionDefinition1() {
        System.out.println("Sad Function Definition 1");
        Recognizer r = new Recognizer("int [] a {}", false);
        try
        {
            r.functionDefinition();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of functionDefinition method, of class Recognizer.
     */
    @Test
    public void sFunctionDefinition2() {
        System.out.println("Sad Function Definition 2");
        Recognizer r = new Recognizer("char a () {}", false);
        try
        {
            r.functionDefinition();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of statement method, of class Recognizer.
     */
    @Test
    public void hTestStatement0() {
        System.out.println("Happy Statement 0");
        Recognizer r = new Recognizer("myNum = -4 + 4", false);
        r.sym.setLexeme("myNum");
        r.sym.setKind(Kind.VARIABLE);
        r.symTable.add(r.sym);
        r.statement();
        Token result = r.lookahead;
        System.out.println(result);
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of statement method, of class Recognizer.
     */
    @Test
    public void hTestStatement1() {
        System.out.println("Happy Statement 1");
        Recognizer r = new Recognizer("if 24 return 69 else return 21", false);
        r.statement();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of statement method, of class Recognizer.
     */
    @Test
    public void hTestStatement2() {
        System.out.println("Happy Statement 2");
        Recognizer r = new Recognizer("while !0 read (myFile)", false);
        r.statement();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of statement method, of class Recognizer.
     */
    @Test
    public void sTestStatement0() {
        System.out.println("Sad Statement 0");
        Recognizer r = new Recognizer("fileName != file1", false);
        r.sym.setLexeme("fileName");
        r.sym.setKind(Kind.VARIABLE);
        r.symTable.add(r.sym);
        try
        {
            r.statement();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of statement method, of class Recognizer.
     */
    @Test
    public void sTestStatement1() {
        System.out.println("Sad Statement 1");
        Recognizer r = new Recognizer("write [ myFile ]", false);
        try
        {
            r.statement();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of statement method, of class Recognizer.
     */
    @Test
    public void sTestStatement2() {
        System.out.println("Sad Statement 2");
        Recognizer r = new Recognizer("return 1 ^ 2", false);

        r.statement();
        assertFalse(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void hTestSimpleExpression0() {
        System.out.println("Happy Simple Expression 0");
        Recognizer r = new Recognizer("1 + 2", false);
        r.simpleExpression();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void hTestSimpleExpression1() {
        System.out.println("Happy Simple Expression 1");
        Recognizer r = new Recognizer("-5 % 9", false);
        r.simpleExpression();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void hTestSimpleExpression2() {
        System.out.println("Happy Simple Expression 2");
        Recognizer r = new Recognizer("+4 * num0 - 11", false);
        r.simpleExpression();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void sTestSimpleExpression0() {
        System.out.println("Sad Simple Expression 0");
        Recognizer r = new Recognizer("-5 / -79 + number7", false);
        try
        {
            r.simpleExpression();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertNotNull(result.getType());
        }
    }
    
    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void sTestSimpleExpression1() {
        System.out.println("Sad Simple Expression 1");
        Recognizer r = new Recognizer("+ 33 36", false);

        r.simpleExpression();
        assertFalse(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of simpleExpression method, of class Recognizer.
     */
    @Test
    public void sTestSimpleExpression2() {
        System.out.println("Sad Simple Expression 2");
        Recognizer r = new Recognizer("+ +", false);

        try{
        r.simpleExpression();
        fail();
        }
        catch(Exception e){
        assertFalse(r.isAtEoF(r.lookahead));
        }
    }

    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void hTestFactor0() {
        System.out.println("Happy Factor 0");
        Recognizer r = new Recognizer("21", false);
        r.factor();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void hTestFactor1() {
        System.out.println("Happy Factor 1");
        Recognizer r = new Recognizer("mySecretSwissBankAccount", false);
        r.factor();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void hTestFactor2() {
        System.out.println("Happy Factor 2");
        Recognizer r = new Recognizer("!(99 > 1)", false);
        r.factor();
        Token result = r.lookahead;
        assertTrue(r.isAtEoF(r.lookahead));
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void sTestFactor0() {
        System.out.println("Sad Factor 0");
        Recognizer r = new Recognizer("9 98", false);

        r.factor();
        assertFalse(r.isAtEoF(r.lookahead));

    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void sTestFactor1() {
        System.out.println("Sad Factor 1");
        Recognizer r = new Recognizer("[ num ]", false);
        try
        {
            r.factor();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertFalse(r.isAtEoF(r.lookahead));
        }
    }
    
    /**
     * Test of factor method, of class Recognizer.
     */
    @Test
    public void sTestFactor2() {
        System.out.println("Sad Factor 2");
        Recognizer r = new Recognizer("myList()", false);
        try
        {
            r.factor();
            fail();
        }
        catch(Exception e)
        {
            Token result = r.lookahead;
            assertFalse(r.isAtEoF(r.lookahead));
        }
    }
}
