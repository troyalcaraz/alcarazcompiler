/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scanner;

import syntaxtree.TokenType;
import java.io.Reader;
import java.io.StringReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author troyalcaraz
 */
public class ScannerTest {
    
    public ScannerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
//
//    /**
//     * Test of yyclose method, of class Scanner.
//     */
//    @Test
//    public void testYyclose() throws Exception {
//        System.out.println("yyclose");
//        Scanner instance = null;
//        //instance.yyclose();
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yyreset method, of class Scanner.
//     */
//    @Test
//    public void testYyreset() {
//        System.out.println("yyreset");
//        Reader reader = null;
//        Scanner instance = null;
//        //instance.yyreset(reader);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yystate method, of class Scanner.
//     */
//    @Test
//    public void testYystate() {
//        System.out.println("yystate");
//        Scanner instance = null;
//        int expResult = 0;
//        int result = instance.yystate();
//        //assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yybegin method, of class Scanner.
//     */
//    @Test
//    public void testYybegin() {
//        System.out.println("yybegin");
//        int newState = 0;
//        Scanner instance = null;
//        instance.yybegin(newState);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yytext method, of class Scanner.
//     */
//    @Test
//    public void testYytext() {
//        System.out.println("yytext");
//        Scanner instance = null;
//        String expResult = "";
//        String result = instance.yytext();
//        //assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yycharat method, of class Scanner.
//     */
//    @Test
//    public void testYycharat() {
//        System.out.println("yycharat");
//        int pos = 0;
//        Scanner instance = null;
//        char expResult = ' ';
//        char result = instance.yycharat(pos);
//        //assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yylength method, of class Scanner.
//     */
//    @Test
//    public void testYylength() {
//        System.out.println("yylength");
//        Scanner instance = null;
//        int expResult = 0;
//        int result = instance.yylength();
//        //assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of yypushback method, of class Scanner.
//     */
//    @Test
//    public void testYypushback() {
//        System.out.println("yypushback");
//        int number = 0;
//        Scanner instance = null;
//        instance.yypushback(number);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }

    /**
     * Test of next method, of class Scanner.
     */
    @org.junit.Test
    public void testNext0() throws Exception {

        String ts = "int num0 = 14+3;";
        
        Scanner instance = new Scanner( new StringReader(ts));
        Token expResult = new Token("int", TokenType.INT);
        
        Token result = instance.next();
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("num0", TokenType.IDENTIFIER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("=", TokenType.EQUAL);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("14", TokenType.NUMBER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("+", TokenType.PLUS);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("3", TokenType.NUMBER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token(";", TokenType.SEMICOLEN);
        assertEquals(expResult, result);
    }
    
    @org.junit.Test
    public void testNext1() throws Exception {

        String ts = "if (2 <= 4) {}";
        
        Scanner instance = new Scanner( new StringReader(ts));
        Token expResult = new Token("if", TokenType.IF);
        
        Token result = instance.next();
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("(", TokenType.LPER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("2", TokenType.NUMBER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("<=", TokenType.LESSEQUAL);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("4", TokenType.NUMBER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token(")", TokenType.RPER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("{", TokenType.LCURLY);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("}", TokenType.RCURLY);
        assertEquals(expResult, result);
    }
    
    @org.junit.Test
    public void testNext2() throws Exception {

        String ts = "while (A != B) {}";
        
        Scanner instance = new Scanner( new StringReader(ts));
        Token expResult = new Token("while", TokenType.WHILE);

        Token result = instance.next();
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("(", TokenType.LPER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("A", TokenType.IDENTIFIER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("!=", TokenType.NOTEQUAL);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("B", TokenType.IDENTIFIER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token(")", TokenType.RPER);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("{", TokenType.LCURLY);
        assertEquals(expResult, result);
        
        result = instance.next();
        expResult = new Token("}", TokenType.RCURLY);
        assertEquals(expResult, result);
    }
    
    @org.junit.Test
    public void testNext3() throws Exception {

        String ts = "9int = 9;";
        Token expResult = new Token("9int", TokenType.IDENTIFIER);
        Scanner instance = new Scanner( new StringReader(ts));
        
        try{
            instance.next();
            fail();
        } 
        catch(Exception e) {
            Token result = instance.next();
            assertNotEquals(expResult, result);
        }
    }
    
    @org.junit.Test
    public void testNext4() throws Exception {

        String ts = "#";
        Token expResult = new Token();
        
        Scanner instance = new Scanner( new StringReader(ts));
        
        try{
            expResult.lexeme = "#";
            expResult.type = null;
        } 
        catch(Exception e) {
            Token result = instance.next();
            assertNotEquals(expResult, result);
        }
    }
    
    @org.junit.Test
    public void testNext5() throws Exception {

        String ts = "14^1";
        
        Scanner instance = new Scanner( new StringReader(ts));
        Token expResult = new Token("14", TokenType.NUMBER);
        
        Token result = instance.next();
        assertEquals(expResult, result);
        
        try{
            result = instance.next();
            assertNull(result.type);
            fail();
        } 
        catch(Exception e) {
            result = instance.next();
            assertNotEquals(expResult, result);
        }
    }
    
}
