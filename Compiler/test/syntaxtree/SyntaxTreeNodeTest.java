
package syntaxtree;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Troy Alcaraz
 */
public class SyntaxTreeNodeTest {
    
    /**
     * Test of indentedToString method, of sytaxTree package.
     */
    @Test
    public void testIndentedToString0() {
        int level = 0;
        OperationNode root = new OperationNode( TokenType.MINUS);
        ValueNode avn = new ValueNode( "3");
        root.setLeft( avn);
        OperationNode opn = new OperationNode( TokenType.MULTIPLY);
        root.setRight( opn);
        avn = new ValueNode( "4");
        opn.setLeft( avn);
        avn = new ValueNode( "5");
        opn.setRight( avn);
        
        //System.out.println("This is the tree that we get right now");
        //System.out.println( root.indentedToString(0));
        
        String expResult = "Operation: MINUS null\n" +
                           "|-- Value: 3 null\n" +
                           "|-- Operation: MULTIPLY null\n" +
                           "|-- --- Value: 4 null\n" +
                           "|-- --- Value: 5 null\n";
        String result = root.indentedToString(level);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testIndentedToString1() {
        int level = 0;
        ProgramNode root = new ProgramNode();
        
        FunctionsDefNode fSN = new FunctionsDefNode();
        
        CompoundStatementNode cSN = new CompoundStatementNode();
        
        DeclarationsNode dSN = new DeclarationsNode();
        
        VariableNode v0 = new VariableNode("dollars");
        dSN.addVariable(v0);
        VariableNode v1 = new VariableNode("yen");
        dSN.addVariable(v1);
        VariableNode v2 = new VariableNode("bitcoins");
        dSN.addVariable(v2);
        
        ValueNode vn0 = new ValueNode("1000000");
        ValueNode vn1 = new ValueNode("104");
        ValueNode vn2 = new ValueNode("6058");
        
        OperationNode op1 = new OperationNode(TokenType.MULTIPLY);
        op1.setLeft(v0);
        op1.setRight(vn1);
        OperationNode op2 = new OperationNode(TokenType.DIVIDE);
        op2.setLeft(v0);
        op2.setRight(vn2);
        
        AssignmentStatementNode asn0 = new AssignmentStatementNode();
        asn0.setLvalue(v0);
        asn0.setExpression(vn0);
        AssignmentStatementNode asn1 = new AssignmentStatementNode();
        asn1.setLvalue(v1);
        asn1.setExpression(op1);
        AssignmentStatementNode asn2 = new AssignmentStatementNode();
        asn2.setLvalue(v2);
        asn2.setExpression(op2);

        root.setFunctions(fSN);
        root.setMain(cSN);
        
        cSN.setVariables(dSN);
        cSN.addStatement(asn0);
        cSN.addStatement(asn1);
        cSN.addStatement(asn2);
        
        String expResult = "Program:\n" +
                           "|-- FunctionDefinitions\n" +
                           "|-- Compound Statement\n" +
                           "|-- --- Declarations\n" +
                           "|-- --- --- Name: dollars null\n" +
                           "|-- --- --- Name: yen null\n" +
                           "|-- --- --- Name: bitcoins null\n" +
                           "|-- --- Assignment\n" +
                           "|-- --- --- Name: dollars null\n" +
                           "|-- --- --- Value: 1000000 null\n" +
                           "|-- --- Assignment\n" +
                           "|-- --- --- Name: yen null\n" +
                           "|-- --- --- Operation: MULTIPLY null\n" +
                           "|-- --- --- --- Name: dollars null\n" +
                           "|-- --- --- --- Value: 104 null\n" +
                           "|-- --- Assignment\n" +
                           "|-- --- --- Name: bitcoins null\n" +
                           "|-- --- --- Operation: DIVIDE null\n" +
                           "|-- --- --- --- Name: dollars null\n" +
                           "|-- --- --- --- Value: 6058 null";
        
        String result = root.indentedToString(level);
        //System.out.println("This is the tree that we get right now");
        System.out.println( result);
        //assertEquals(expResult, result);
    }
    
    @Test
    public void testIndentedToString2() {
        int level = 0;
        ProgramNode root = new ProgramNode();
        
        FunctionsDefNode fSN = new FunctionsDefNode();
        
        FunctionDefNode fn = new FunctionDefNode("grades");
        
        VariableNode vn0 = new VariableNode("assign0");
        VariableNode vn1 = new VariableNode("assign1");
        VariableNode vn2 = new VariableNode("assign2");
        
        CompoundStatementNode empty = new CompoundStatementNode();
        DeclarationsNode empty2 = new DeclarationsNode();
        empty.setVariables(empty2);
        
        fn.setReturnType(DataType.INT);
        fn.addParameter(vn0);
        fn.addParameter(vn1);
        fn.addParameter(vn2);
        fn.setBody(empty);
        
        fSN.addFunctionDefinition(fn);
        
        CompoundStatementNode cSN = new CompoundStatementNode();
        
        DeclarationsNode dSN = new DeclarationsNode();
        
        VariableNode v0 = new VariableNode("a");
        dSN.addVariable(v0);
        VariableNode v1 = new VariableNode("b");
        dSN.addVariable(v1);
        VariableNode v2 = new VariableNode("c");
        dSN.addVariable(v2);
        VariableNode v3 = new VariableNode("d");
        dSN.addVariable(v3);
        
        cSN.setVariables(dSN);
        
        root.setFunctions(fSN);
        root.setMain(cSN);
        
        String expResult = "Program:\n" +
                           "|-- FunctionDefinitions\n" +
                           "|-- --- Function: grades returns INT\n" +
                           "|-- --- --- Name: assign0 null\n" +
                           "|-- --- --- Name: assign1 null\n" +
                           "|-- --- --- Name: assign2 null\n" +
                           "|-- --- --- Compound Statement\n" +
                           "|-- --- --- --- Declarations\n" +
                           "|-- Compound Statement\n" +
                           "|-- --- Declarations\n" +
                           "|-- --- --- Name: a null\n" +
                           "|-- --- --- Name: b null\n" +
                           "|-- --- --- Name: c null\n" +
                           "|-- --- --- Name: d null";
        
        String result = root.indentedToString(level);
        //System.out.println("This is the tree that we get right now");
        System.out.println( result);
        //assertEquals(expResult, result);
    }
}
