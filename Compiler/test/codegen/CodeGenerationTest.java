
package codegen;

import org.junit.Test;
import static org.junit.Assert.*;
import parser.Parser;
import syntaxtree.*;
import analyzer.SemanticAnalyzer;

/**
 *
 * @author troyalcaraz
 */
public class CodeGenerationTest {

    /**
     * Test of writeCodeForRoot method, of class CodeGeneration with
     * an ExpressionNode inside.
     */
    @Test
    public void testWriteCodeForRoot() {
        System.out.println("writeCodeForRoot");
        Parser p = new Parser("money.ac", true);
        ProgramNode root = p.program();
        SemanticAnalyzer sa = new SemanticAnalyzer(root, p.symTable);
        sa.checkDeclarations();
        sa.assignDataType();
        sa.checkAssignmentTypes();
        CodeGeneration instance = new CodeGeneration();
        String expResult = ".data\n" +
                            "dollars:   .word 0\n" +
                            "yen:       .word 0\n" +
                            "bitcoins:  .word 0\n" +
                            "num:       .word 0\n" +
                            "pi:        .float 0.0\n" +
                            "\n" +
                            "\n" +
                            ".text\n" +
                            "main:\n" +
                            "li.s   $f30,   3.14\n" +
                            "swc1   $f30,   pi\n" +
                            "li     $s0,   1000000\n" +
                            "sw     $s0,   dollars\n" +
                            "lw     $t0,   dollars\n" +
                            "li     $t1,   104\n" +
                            "mult   $t0,   $t1\n" +
                            "mflo   $s0\n" +
                            "sw     $s0,   yen\n" +
                            "lw     $t0,   dollars\n" +
                            "li     $t1,   6058\n" +
                            "div    $t0,   $t1\n" +
                            "mflo   $s0\n" +
                            "sw     $s0,   bitcoins\n" +
                            "li     $t2,   2\n" +
                            "li     $t4,   3\n" +
                            "li     $t5,   7\n" +
                            "mult   $t4,   $t5\n" +
                            "mflo   $t3\n" +
                            "add    $t1,   $t2,   $t3\n" +
                            "li     $t3,   5\n" +
                            "li     $t4,   2\n" +
                            "div    $t3,   $t4\n" +
                            "mflo   $t2\n" +
                            "sub    $t0,   $t1,   $t2\n" +
                            "li     $t1,   18\n" +
                            "add    $s0,   $t0,   $t1\n" +
                            "sw     $s0,   num\n" +
                            "li     $v0,   10\n" +
                            "syscall\n";
        String result = instance.writeCodeForRoot(root);
//        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of writeCode method, of class CodeGeneration.
     */
    @Test
    public void testWriteCode_OperationNode_String() {
        System.out.println("writeCode");
        Parser p = new Parser("(76 < 100)", false);
        OperationNode opNode = (OperationNode) p.expression();
        opNode.setType(DataType.INT);
        opNode.getLeft().setType(DataType.INT);
        opNode.getRight().setType(DataType.INT);
        String resultRegister = "$s0";
        CodeGeneration instance = new CodeGeneration();
        String expResult = "li     $t0,   76\n" +
                            "li     $t1,   100\n" +
                            "blt    $t0,   $t1,   lesser0\n" +
                            "li     $s0,   0\n" +
                            "j      lessExit0\n" +
                            "lesser0:\n" +
                            "li     $s0,   1\n" +
                            "lessExit0:\n";
        String result = instance.writeCode(opNode, resultRegister);
//        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of writeCode method, of class CodeGeneration.
     */
    @Test
    public void testWriteCode_ValueNode_String() {
        System.out.println("writeCode");
        Parser p = new Parser("69", false);
        ValueNode valNode = (ValueNode) p.expression();
        String resultRegister = "$s0";
        valNode.setType(DataType.INT);
        CodeGeneration instance = new CodeGeneration();
        String expResult = "li     $s0,   69\n";
        String result = instance.writeCode(valNode, resultRegister);
//        System.out.println(result);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testWriteCode_IfStatement_String() {
        System.out.println("writeCodeForIf");
        Parser p = new Parser("iftest.ac", true);
        ProgramNode pn = p.program();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn, p.symTable);
        sa.checkDeclarations();
        sa.assignDataType();
        sa.checkAssignmentTypes();
        CodeGeneration instance = new CodeGeneration();
        String expResult = ".data\n" +
                            "aaa:       .float 0.0\n" +
                            "bbb:       .float 0.0\n" +
                            "iii:       .float 0.0\n" +
                            "\n" +
                            "\n" +
                            ".text\n" +
                            "main:\n" +
                            "li.s   $f30,   10.0\n" +
                            "swc1   $f30,   aaa\n" +
                            "lwc1   $f0,   aaa\n" +
                            "li.s   $f2,   15.0\n" +
                            "c.lt.s $f0,   $f2\n" +
                            "bc1t   floatLesser0\n" +
                            "li     $s0,   0\n" +
                            "j      floatLessExit0\n" +
                            "floatLesser0:\n" +
                            "li     $s0,   1\n" +
                            "floatLessExit0:\n" +
                            "beq   $s0,   $zero, elseCode0\n" +
                            "li.s   $f30,   6.0\n" +
                            "swc1   $f30,   bbb\n" +
                            "lwc1   $f0,   bbb\n" +
                            "li.s   $f2,   2.0\n" +
                            "add.s  $f30,   $f0,   $f2\n" +
                            "mov.s  $f12,  $f30\n" +
                            "li     $v0,   2\n" +
                            "syscall\n" +
                            "j endIf0\n" +
                            "elseCode0:\n" +
                            "endIf0:\n" +
                            "li.s   $f30,   0.0\n" +
                            "swc1   $f30,   iii\n" +
                            "while0:\n" +
                            "lwc1   $f0,   iii\n" +
                            "li.s   $f2,   10.0\n" +
                            "c.lt.s $f0,   $f2\n" +
                            "bc1t   floatLesser1\n" +
                            "li     $s0,   0\n" +
                            "j      floatLessExit1\n" +
                            "floatLesser1:\n" +
                            "li     $s0,   1\n" +
                            "floatLessExit1:\n" +
                            "beq   $s0,   $zero, exit0\n" +
                            "lwc1   $f0,   iii\n" +
                            "li.s   $f2,   1.0\n" +
                            "add.s  $f30,   $f0,   $f2\n" +
                            "swc1   $f30,   iii\n" +
                            "j      while0\n" +
                            "exit0:\n" +
                            "li     $v0,   10\n" +
                            "syscall\n";
        String result = instance.writeCodeForRoot(pn);
//        System.out.println(result);
        assertEquals(expResult, result);
    }
}
