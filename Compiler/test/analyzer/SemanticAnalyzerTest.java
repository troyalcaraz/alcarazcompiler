
package analyzer;

import org.junit.Test;
import static org.junit.Assert.*;
import parser.Parser;
import symboltable.SymbolTable;
import syntaxtree.*;

/**
 *
 * @author Troy Alcaraz
 */
public class SemanticAnalyzerTest {
    
    /**
     *
     */
    public SemanticAnalyzerTest() {
    }

    /**
     * Test of checkDeclarations method, of class SemanticAnalyzer.
     */
    @Test
    public void testCheckDeclarations0() {
        System.out.println("Happy checkDeclarations");
        Parser p = new Parser("money.ac", true);
        ProgramNode pn = p.program();
        SymbolTable st = p.symTable;
        SemanticAnalyzer instance = new SemanticAnalyzer(pn, st);
        instance.checkDeclarations();
        boolean result = instance.getCanWriteAssembly();
        boolean expected = true;
        
        assertEquals(expected, result);
    }
    
    /**
     * Test of checkDeclarations method, of class SemanticAnalyzer.
     */
    @Test
    public void testCheckDeclarations1() {
        System.out.println("Sad checkDeclarations");
        Parser p = new Parser("badmoney.ac", true);
        ProgramNode pn = p.program();
        SymbolTable st = p.symTable;
        SemanticAnalyzer instance = new SemanticAnalyzer(pn, st);
        instance.checkDeclarations();
        boolean result = instance.getCanWriteAssembly();
        boolean expected = false;
        
        assertEquals(expected, result);
    }

    /**
     * Test of assignDataType method, of class SemanticAnalyzer.
     */
    @Test
    public void testAssignDataTypes0() {
        System.out.println("Happy assignDataTypes 0");
        Parser p = new Parser("iftest.ac", true);
        ProgramNode pn = p.program();
        SymbolTable st = p.symTable;
        SemanticAnalyzer instance = new SemanticAnalyzer(pn, st);
        instance.assignDataType();
        boolean result = instance.getCanWriteAssembly();
        boolean expected = true;
        
        assertEquals(expected, result);
    }
    
    /**
     *
     */
    @Test
    public void testAssignDataTypes1() {
        System.out.println("Sad assignDataTypes");
        Parser p = new Parser("badmoney.ac", true);
        ProgramNode pn = p.program();
        SymbolTable st = p.symTable;
        SemanticAnalyzer instance = new SemanticAnalyzer(pn, st);
        instance.assignDataType();
        boolean result = instance.getCanWriteAssembly();
        boolean expected = false;
        
        assertEquals(expected, result);
    }

    /**
     * Test of checkAssignmentTypes method, of class SemanticAnalyzer.
     */
    @Test
    public void testCheckAssignmentTypes0() {
        System.out.println("Happy checkAssignmentTypes");
        Parser p = new Parser("money.ac", true);
        ProgramNode pn = p.program();
        SymbolTable st = p.symTable;
        SemanticAnalyzer instance = new SemanticAnalyzer(pn, st);
        instance.assignDataType();
        instance.checkDeclarations();
        boolean result = instance.getCanWriteAssembly();
        boolean expected = true;
        
        assertEquals(expected, result);
    }
    
    /**
     *
     */
    @Test
    public void testCheckAssignmentTypes1() {
        System.out.println("Sad checkAssignmentTypes");
        Parser p = new Parser("badmoney.ac", true);
        ProgramNode pn = p.program();
        SymbolTable st = p.symTable;
        SemanticAnalyzer instance = new SemanticAnalyzer(pn, st);
        instance.assignDataType();
        instance.checkAssignmentTypes();
        boolean result = instance.getCanWriteAssembly();
        boolean expected = false;
        
        assertEquals(expected, result);
    }
}
