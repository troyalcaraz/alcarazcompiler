/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symboltable;

import org.junit.Test;
import static org.junit.Assert.*;
import syntaxtree.DataType;

/**
 *
 * @author troyalcaraz
 */
public class SymbolTableTest {
    
    public SymbolTableTest() {
    }

    /**
     * Test of add method, of class SymbolTable.
     */
    @Test
    public void hTestAdd() {
        System.out.println("Happy add");
        SymbolTable st = new SymbolTable();
        SymbolTable.Symbol s = st.new Symbol("intVariable", Kind.VARIABLE, DataType.INT);
        st.add(s);
        boolean expResult = true;
        boolean result = st.symbolTable.containsKey(s.getLexeme());
        assertEquals(expResult, result);
    }
    
    @Test
    public void sTestAdd() {
        System.out.println("Happy add");
        SymbolTable st = new SymbolTable();
        Object expResult = null;
        SymbolTable.Symbol result = st.symbolTable.get("intVariable");
        assertEquals(expResult, result);
    }

    /**
     * Test of kind method, of class SymbolTable.
     */
    @Test
    public void hTestKind() {
        System.out.println("Happy kind");
        SymbolTable st = new SymbolTable();
        SymbolTable.Symbol s = st.new Symbol("intVariable", Kind.VARIABLE, DataType.INT);
        st.add(s);
        String expResult = "VARIABLE";
        String result = st.kind(s.getLexeme());
        assertEquals(expResult, result);
    }
    
    @Test
    public void sTestKind() {
        System.out.println("Happy kind");
        SymbolTable st = new SymbolTable();
        SymbolTable.Symbol s = st.new Symbol("intVariable", Kind.VARIABLE, DataType.INT);
        st.add(s);
        String expResult = "ARRAY";
        String result = st.kind(s.getLexeme());
        assertNotEquals(expResult, result);
    }
}
