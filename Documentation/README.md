Troy Alcaraz

In this project is an almost c compiler. The parts of a
compiler we will be making and testing are a scanner, a two part parser, semantic
analyzer, and assembly code generator. This compiler will take an almost c source code 
file and generator assembly code for it.

In this project you will find three distint folders, Compiler, Documentation, and a
Product folder. In the Compiler folder are all of the netBeans files, and most importantly 
the src file and test file with the source code and test code for all the java and class
files for the compiler. You will also find some sample almost c files that were used to 
test the compiler while building it. Next is the Documentation folder, where you find this
read me, and a Software Design Document as both a docx and pdf. This read me will break
down what you will all find inside of the compiler package and where to find them. The SDD
will break down and explain exactly how the compiler works. Lastly is the Product folder 
where you will find a user manual as both a docx and pdf, and most importantly a compiler 
jar file. The user manual will show you how to use the jar file on the command line to 
start plugging almost c files into to generate assembly code. It also include so some
helpful hints about some of the most common errors you may run into and what to look for 
to correct any errors.

Bellow is the break down of each file that can be found in this package.

Compiler
	- badmoney.ac
	- build.xml
	- iftest.ac
	- manifest.mf
	- money.ac
	- nbproject
		- build-impl.xml
		- genfiles.properties
		- project.properties
		- project.xml
	- src
		- analyzer
			- SemanticAnalyzer.java
		- codegen
			- CodeGeneration.java
		- compiler
			- CompilerMain.java
		- parser
			- parser.iml
			- Parser.java
			- Recognizer.java
		- scanner
			- jflex.jar
			- LookupTable.java
			- Scanner.java
			- scanner.jflex
			- ScannerMain.java
			- Token.java
		- symboltable
			- Kind.java
			- SymbolTable.java
		- syntaxtree
			- AssignmentSatementNode.java
			- CompoundStatementNode.java
			- DataType.java
			- DeclarationsNode.java
			- ExpressionListNode.java
			- ExpressionNode.java
			- FunctionDecNode.java
			- FunctionDefNode.java
			- FunctionsDecNode.java
			- FunctionsDefNode.java
			- IdentifierListNode.java
			- IfStatementNode.java
			- OperationNode.java
			- ParametersNode.java
			- ProcedureStatementNode.java
			- ProgramNode.java
			- ReadStatementNode.java
			- ReturnStatementNode.java
			- StatementListNode.java
			- StatementNode.java
			- SyntaxTreeNode.java
			- TokenType.java
			- ValueNode.java
			- VariableNode.java
			- WhileStatementNode.java
			- WriteStatementNode.java
	- test
		- analyzer
			- SemanticAnalyzerTest.java
		- codegen
			- CodeGenerationTest.java
		- parser
			- ParserTest.java
			- RecognizerTest.java
		- scanner
			- ScannerTest.java
		- symboltable
			- SymbolTableTest.java
		- syntaxtree
			- SyntaxTreeNodeTest.java
Documentation
	- README.md
	- SDD.docx
	- SDD.pdf
Product
	- Compiler.jar
	- UserManual.docx
	- UserManual.pdf
